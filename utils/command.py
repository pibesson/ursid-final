import os, subprocess, logging

log = logging.getLogger(__name__)


def commande(cmd: str) -> str:
    """
    Execute a simple command in shell
    simple => "cmd arg1 arg2 arg3"

    Args:
        string:   `cmd`               ->    command

    Returns:
        string:   `command_output`    ->    command output
    """
    # log.debug(f"Executed `{cmd}`")
    cmd = cmd.split()
    shell_cmd = subprocess.run((cmd), capture_output=True, text=True)
    command_output = shell_cmd.stdout
    # log.debug(f"Returned: \n{command_output}")
    return command_output


def command_complex(cmd: str) -> str:
    """
    Execute a complex command in shell

    Args:
        string:   `cmd`               ->    command

    Returns:
        string:   `command_output`    ->    command output
    """
    # log.debug(f"Executed `{cmd}`")
    shellcmd = os.popen(cmd)
    command_output = shellcmd.read()
    # log.debug(f"Returned: \n{command_output}")
    return command_output
