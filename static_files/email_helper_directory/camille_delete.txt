Bonjour M. Ocean,

Je reviens vers vous car je n'ai toujours aucune réponse de mes emails adressés à admin@casinolimit.bzh. Est ce que ce compte est vraiment lu ?

Je souhaite effacer mes données personnelles sur votre système d'information, conformément à l'article 17.1 du règlement général sur la protection des données (RGPD).

Cela fait plusieurs mois que je vous fais cette demande alors même que je ne fréquente plus votre casino.

Sans réponse et action de votre part, je devrais me résoudre à faire cela par moi-même.

En vous souhaitant une bonne fin de journée,

C@m1lle -- Ethical hacker
