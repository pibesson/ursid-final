# Adding procedures
Adding new procedures requires an understanding of how architectural constraints are written in order to add your own.
Additionally, looking into the provided examples and in the documentation of modules chouchen.constraints and chouchen.subconstraints may also help.

Note that adding new procedures also require engineering work in Apiculteur if you wish to deploy them.

## OS
Some procedures may only function on specific OS versions, which has to be specified into the procedure architectural constraints.
For instance, [CVE-2021-4034](https://nvd.nist.gov/vuln/detail/cve-2021-4034) is natively exploitable on Ubuntu 16.04, but not Ubuntu 20.04.
Such a constraint can be specified with
```
"OS": [
        {
          "Type": "canonical:ubuntu_linux",
          "Version": "==16.04"
        }
      ],
```
Notes:
- OS names are written as vendor:product_name, according to the [CPE](https://nvd.nist.gov/products/cpe) standard.
- URSID natively supports Ubuntu 16.04, 20.04 and Debian 10.0. Additional Ubuntu and Debian versions may be supported 
(as long as they are available on Vagrant) by editing the OS_VERSIONS array in houchen.subconstraints.OS_type, but may result in bugs when deploying some procedures.
Additional OS types (such as different UNIX versions or Windows) are currently not supported.
- If your procedure requires linux but works on any distro (for instance a .bash_history leak), you may write the OS as just "linux".

## Account
Some procedures may require specific account to be created or edited to be exploited. Currently supported format is as follows:
- _Name_ can be a specific hardcoded value, "EXIT_USER" if it should correspond to the user attacked by the procedure, or "ENTRY_USER" if it should correspond to the user initiating the procedure.
- _Group_ is the same as name.
- _Privilege_ can be either "User", "Superuser" or "*". Superuser means the user means will have sudo privileges - useful for instance for privilege escalation procedures.
- _Credentials_ sets the user credentials:
    - _Credential Type_ specifies which kind of credential it is, leave at "PLAINTEXT_PASSWORD".
    - _Requires Secret_ asks whether the user's credential is linked to a secret in the scenario.
    - If _Requires Secret_ is true, "Secret Type" specifies the type of secret it should be linked to (usually "PLAINTEXT_PASSWORD).
    - If _Requires Secret_ is true, "Secret Amount" specifies how many secrets it can be linked to (usually "*").

For instance, an account could be setup to have its password be the same as a plaintext password leaked earlier in a secret in the scenario.
Such a constraint can be specified with
```
 "Account": [
        {
          "Name": "EXIT_USER",
          "Group": "EXIT_USER",
          "Privilege": "*",
          "Credentials": {
            "Credential Type": "PLAINTEXT_PASSWORD",
            "Requires Secret": true,
            "Secret Type": "PLAINTEXT_PASSWORD",
            "Secret Amount": "*"
          }
        }
      ]
```

## Software
Some procedures may require software to be installed, sometime on specific versions.
For instance, [CVE-2019-14287](https://nvd.nist.gov/vuln/detail/CVE-2019-14287) requires a specific version of the sudo package to be installed.
Such a constraint can be specified with
```
"Software": [
        {
          "Type": "sudo_project:sudo",
          "Version": "==1.8.27",
          "Port": "-1"
        }
      ]
```
Notes:
- Just like OS, Software names are written as vendor:product_name, according to the [CPE](https://nvd.nist.gov/products/cpe) standard.
- Port is optional when not needed (default value is -1).
 
## File
Some procedures may require specific files to be created or altered to be exploited. Currently supported format is as follows:
- _Path_ is the path the file will have on the machine.
- _Permissions_ specifies file permissions:
    - _User_ is the owner of the file. Can be a specific value, ENTRY_USER or EXIT_USER.
    - _Group_ is similar.
    - _Perm_ is file permissions written in 4 digit UNIX octal format.
- Content specifies the content of the file:
    - _Content Type_ is a hardcoded value corresponding to what will actually be written inside. 
    Consult chouchen.file_generation for more information.
    - _Requires Secret_ asks whether the files content are linked to a secret in the scenario.
    - If _Requires Secret_ is true, "Secret Type" specifies the type of secret it should be linked to.
    - If _Requires Secret_ is true, "Secret Amount" specifies how many secrets it can be linked to (usually "*").

## Secret
Some procedures may require or reward secrets. This needs to be specified in order for the scenario to be coherent and beatable.
If you wish to know more about how secrets are generated and handled, refer to [](#on_secret_generation)

You need to specify which kind of secret can be required or reward, and how many.
The list of all currently available secrets is in chouchen.secret_generation.generate_secret(). This is also the function you need to edit if you wish to add more types.
For instance, to specify that a procedure require a single SSH key secret:

```
Secret Preconditions": {
      "Requires": [
        {
          "Type": "SSH_KEYS",
          "Amount": "1"
        }
      ],
      "Rewards": []
    }
```