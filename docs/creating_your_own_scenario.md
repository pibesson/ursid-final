# Creating your own scenario

You may edit the main_scenario function in main.py in order to make URSID run on your own scenario instead.
This is done by, in order:

- Creating every attack position in your scenario using the [Node](chouchen_api:chouchen.system_graph
  )
  class.
- Creating every transition in your scenario using the [Transition](chouchen_api:chouchen.system_graph) class and
  ATT&CK technique names.
- Choosing which attack positions are starting or winning.
- Creating a scenario using the [SystemGraph](chouchen_api:chouchen.system_graph) class.

You are constrained to which techniques are available in ./json/procedures directory.
You may add a new technique by creating a new json file with the appropriate name.
Refer to [](#adding_procedures) for adding procedures.

Then, to save your scenario as a json at OUTPUT_PATH, run

```
python3 main.py OUTPUT_PATH --generate_scenario_json
```

## Additional configurations

URSID now supports a few additional configurations to embelish your scenario,
including adding ornament users or specifying your available IP range.
If your scenario is named "scenario.json", additional configuration must be written in a file named "scenario.yml".
Options are written for each machine. Note that picking specific options may influence which procedures can be chosen
for consistency reasons: adding options here adds initial constraints to the machines during the refinement process.

Currently available options are:

- options:
    - auditd: installs an auditd package and logging system by default.
    - casinolimit_banner: changes the motd on the machine (used for breizh_ctf).
    - dns_server: makes this machine a dns server.
- dns_server: specify which network the DNS will be for. The network name MUST be in the form of network + a number, for
  instance network1.
- ornament_users: provide a list of username which will be additionally installed on the machine. These users will not
  take part of the scenario and are purely for cosmetic purposes.
- secrets: hardcode specific values for secrets within the scenario. Only works for plaintext passwords. This can be put
  as a specification of any of the machines.
- networks: specify name of the network of the machine. By default every machine will run on the same subnetwork, this
  gives you more control on the structure of your subnets. Network names MUST be network1, network2, ...

For example, here is a configuration for a scenario with two machines:
TODO: REWRITE THIS DOC

- machine1 is a dns server for machine2.
- machine1 has one network interface, machine2 2.
- machine1 has an additional user installed (johnny).
- The scenario has a secret named "password" which will have the value of toto.

```yaml
machine1:
  options:
    - auditd
    - dns_server
  networks:
    - network1
  ornament_users:
    - johnny
  secrets:
    - password:
        toto
machine2:
  options:
    - auditd
  dns_server:
    machine1
  networks:
    - network1
    - network2
```

