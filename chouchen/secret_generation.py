"""Module responsible for generating secrets.
This module generates needed secrets during the scenario, such as SSH key pairs or credentials.
Such credentials need to have consistent values throughout the scenario. For instance, the password leaked in a text
file on one machine needs to match the password of a user in the other.
These cases are handled by the secret_dictionary. This dictionary associates the name of each secret (according to
the technical scenario) to a Secret object, which gives it a type (ssh key, password...) and a value.
At the end of the refinement process, every object requiring secrets in the scenario (files, accounts...) is generated
by checking which secret names are associated to it.
If the secret hasn't been generated before (no value in the Secret object), then its value will be generated according
to its type. Otherwise, the previously generated value will be used. This ensures consistency throughout the whole
scenario.

"""

import random as rd
import secrets

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

from chouchen import subconstraints, system_graph


class Secret:
    """A class to represent Secret objects, which are responsible for handling credentials and other data needed
    by the attacker to progress.
    OS constraints contain a type (Windows, Ubuntu...) and a version (<=20.04, ==16.04...).

    Current limitations:
        - A procedure may only have a single OS constraint. If you want to have a procedure that can work on Ubuntu
        AND Debian, you will have to create 2 different procedures.
        - If you want to specify specific versions (such as 16.04 OR 20.04), you will have to write it as an interval
        instead (>=16.04, <=20.04).
    Attributes:
        name (str): The name of the secret (as written in the technical scenario).
        secret_type (subconstraints.SecretType): The type of secret it is (PLAINTEXT_PASSWORD, SSH_KEYS...)
        value (str): The value of the secret. May be empty at first before the secret is properly generated.
        related_nodes (list[str]): The list of all nodes in the scenario that have this secret name in them.
    """

    def __init__(self, name: str, secret_type: "subconstraints.SecretType", value: str = None) -> None:
        self.name = name
        self.secret_type = secret_type
        self.value = value
        self.related_nodes = []

    def add_related_nodes(self, scenario: "system_graph.SystemGraph") -> None:
        """Goes through the scenario and gets every transition requiring or rewarding this secret based on its name.

        Args:
            scenario: the related scenario.
        """
        related_transitions = scenario.find_transitions_requiring_secret(self.name)
        self.related_nodes += [transition.exit_node for transition in related_transitions]


def generate_secret(secret_name: str, secret_dictionary: dict) -> str:
    """Generates a secret based on its value entry, by finding it in the secret_dictionary using secret_name as the key.
    Raises a ValueError if the secret_type of the secret corresponding to the name is not accepted.
    Args:
        secret_name(str): the secret to generate.
        secret_dictionary(dict): the secret dictionary.

    Returns:
        The value of the secret.
    """
    # Find the secret in the dictionary, raising an error if it doesn't exist
    if secret_name not in secret_dictionary:
        raise ValueError("Secret not found in dictionnary, aborting...")
    # Then gives it a value if it doesn't have one already, according to its secret type
    secret = secret_dictionary[secret_name]
    if secret.value:  # isn't None
        return secret.value
    if secret.secret_type.secret_subtype:
        match secret.secret_type.secret_subtype:
            case "FIXED_CAMERA_PASSWORD":
                secret.value = "kaeCaiSoojie7i"
                secret.secret_type = "PLAINTEXT_PASSWORD"  # necessary for compatibility reasons
                return secret.value

    else:
        match secret.secret_type.secret_type:
            case "PLAINTEXT_PASSWORD":
                secret.value = "plaintext_password" + str(rd.randint(0, 100000))
                return secret.value

            case "FIXED_CAMERA_PASSWORD":
                secret.value = "fixed_camera_password"
                secret.secret_type = "PLAINTEXT_PASSWORD"  # necessary for compatibility reasons
                return secret.value

            case "EMAIL_ADDRESS":
                # NOTE: For now this is hardcoded to be relevant to the BreizhCTF scenario.
                return "admin@casinolimit.bzh"
            case "SSH_KEYS":
                private_key = rsa.generate_private_key(
                    public_exponent=65537,
                    key_size=3072
                )

                encrypted_pem_private_key = private_key.private_bytes(
                    encoding=serialization.Encoding.PEM,
                    format=serialization.PrivateFormat.OpenSSH,
                    encryption_algorithm=serialization.NoEncryption()
                )

                pem_public_key = private_key.public_key().public_bytes(
                    encoding=serialization.Encoding.OpenSSH,
                    format=serialization.PublicFormat.OpenSSH
                )

                secret.value = encrypted_pem_private_key.decode() + "|" + pem_public_key.decode()
                return secret.value

            case "CRACKABLE_PASSWORD":
                # to be upgraded in the future
                with open("../static_files/password_lists/rockyou_512.txt", "r") as inFile:
                    rockyou_list = inFile.read()
                    rockyou_list = rockyou_list.splitlines()
                secret.value = rd.choice(rockyou_list)
                return secret.value

            case "FLAG_INSIDE_DATABASE":
                # to be upgraded in the future
                secret.value = "a placeholder database flag" + str(rd.randint(0, 100000))
                return secret.value

            case _:
                raise ValueError(f"Error: Unhandled secret type {secret.secret_type.secret_type}")


def generate_credential(credential_type: str) -> str:
    """Generates credentials not related to any secret.
    Useful when giving a default value to passwords for users.
    Raises a ValueError if the credential_type is not accepted.

    Args:
        credential_type: the type of credential to be generated.

    Returns:
        The value of the credential.
    """
    match credential_type:
        case "PLAINTEXT_PASSWORD":
            return "a plaintext password not from a secret"
        case "RANDOM_WEAK_PASSWORD":
            return "dragon"  # TODO: make this actually random
        case "FIXED_NPM_BREIZH_CTF_PASSWORD":
            return "npm_password_455745842"  # fixed value for breizh ctf
        case "*":  # default password at strong value
            # yes the fact this python module is named secrets is confusing
            return secrets.token_urlsafe(16)
        case _:
            raise ValueError
