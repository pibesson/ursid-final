# -*- coding: utf-8 -*-
"""System Graph definitions.

This module codifies what a scenario is on a technical level, by representing them as attack graphs
(nodes and transitions).

This module contains the following classes:
    - SystemGraph, the scenario itself.
    - Node, the nodes of the attack graph. Represented as a tuple of (machine, user) representing an attack position.
    - Transition, the transitions of the attack graph. Labelled using MITRE ATT&CK techniques and/or procedures.

This module also provides basic simulation of the path of an attacker throughout the scenario:
    - AttackState, representing the state of an attacker (controlled nodes, acquired secrets) at a given time.
    - AttackPath, representing the journey of an attacker through the scenario (list of attack states).

This module can also import or export scenarios in a json format.
Scenarios generated using this module may then be represented using the graphics module, or refined into procedural
scenarios using the procedural_refinement module.
"""

import copy
import itertools
import json
import logging
import random as rd
from typing import Tuple

import chouchen.constraints

project_name = "System Graph"

log = logging.getLogger(project_name)


class NoValidTransitionsError(Exception):
    """Raised when an attacker has no valid moves left."""
    pass


class InvalidTransitionError(Exception):
    """Raised when an attacker attempts to take a transition he is not actually able to."""
    pass


class SystemGraph:
    """
        A class used to represent an attack scenario through a System Graph.

        Attributes:
            nodes (list[Node]): The list of all attack positions in the scenario.
            starting_nodes (list[Node]): The list of all starting attack positions in the scenario.
            winning_nodes (list[Node]): The list of all winning positions in the scenario.
            transitions  (list[Transition]): The list of all transitions available between attack positions.
        """

    def __init__(self, nodes: list["Node"], starting_nodes: list["Node"],
                 winning_nodes: list["Node"], transitions: list["Transition"]) -> None:
        self.nodes = nodes
        self.starting_nodes = starting_nodes
        self.winning_nodes = winning_nodes
        self.transitions = transitions

        self.additional_config = {}  # This is empty by default, main can fill it if the yml file exists

    def is_winnable(self) -> Tuple[bool, list["Transition"]]:
        """Checks whether the scenario is winnable or not.
        This is done by bruteforcing every transition available until either the scenario is won or
        there is no transition left and by only taking transitions that lead to a different attack state.
        This also calculates a winning attack path at random (list of transitions taken by the attacker leading to his
        winning position).

        Returns:
            (True, list[Transitions]) if the scenario is winnable, (False, []) otherwise.
        """

        # Generate an initial attack state
        init_attack_state = AttackState.default_attack_state(self)

        # Take random actions until either an error is raised or the scenario is beaten
        def sub_exploration(sys_graph: "SystemGraph", attack_state: AttackState, current_path: list["Transition"]):
            """Recursively explores the scenario by taking random valid transitions at every step.
            These transitions also have to improve the situation of the attacker, by either giving him new nodes
            or secrets. This garantees that this algorithm will end.
            This algorithm ends either when a winning attack path is found, or no useful and valid transitiosn are left.

            Args:
                sys_graph: The scenario.
                attack_state: The current attack state.
                current_path: The current list of transitions taken.

            Returns:
                A boolean indicating whether the search of a winning position was successful,
                and the list of transitions leading to this situation.
            """
            if attack_state.is_winning(sys_graph):
                return True, current_path
            legal_transitions = []
            # get all possible transitions
            for transition in sys_graph.transitions:
                if attack_state.is_transition_valid(transition):
                    legal_transitions.append(transition)
            if len(legal_transitions) == 0:
                return False, []
            for transition in legal_transitions:
                new_attack_state = copy.deepcopy(attack_state)
                new_attack_state.take_transition(transition)
                if new_attack_state.nodes != attack_state.nodes or new_attack_state.secrets != attack_state.secrets:
                    return sub_exploration(sys_graph, new_attack_state, current_path + [transition])
            return False, []

        result, transitions = sub_exploration(self, init_attack_state, [])
        return result, transitions

    def find_transition_index(self, transition: "Transition") -> int:
        """Returns the index of the transition equal to the given argument. Returns -1 if not found.
        2 transitions are equal if they have the same transition_id.

        Args:
            transition (Transition): a transition object

        Returns:
            The index of the transition in self.transitions
        """

        for i in range(len(self.transitions)):
            if self.transitions[i].transition_id == transition.transition_id:
                return i
        return -1

    def find_transitions_requiring_secret(self, secret_name: str) -> list["Transition"]:
        """Gets all transitions where secret_name is a requirement.
        Useful for secret shenanigans later when we need to get all related users.

        Args:
            secret_name: The name of the secret to find

        Returns:
            The list of all transitions where the secret_name is in the required secrets.
        """
        return [transition for transition in self.transitions if secret_name in transition.requires]

    def add_node_to_scenario(self, machine: str, user: str, is_starting: bool, is_winning: bool) -> None:
        """Adds a node to the scenario. This node will initially have no transitions attached.
        Useful for GUI applications.

        Args:
            machine: the machine name of the attack position.
            user: the user name of the attack position.
            is_starting: whether this is a starting node.
            is_winning: whether this is a winning node.

        """
        new_node = Node(machine, user, self.get_new_node_id())
        # Check if node already exists + get list of current ids

        for node in self.nodes:
            if node.machine == machine and node.user == user:
                log.info("Node already exists")
                return
        self.nodes.append(new_node)
        if is_starting:
            self.starting_nodes.append(new_node)
        if is_winning:
            self.winning_nodes.append(new_node)

    def remove_node_from_scenario(self, node: "Node"):
        """Removes a node from the scenario based on a string representation.
        Note that this will also remove any transition associated with that node.

        Args:
            node: a Node

        """
        machine = node.machine
        user = node.user

        new_nodes = [x for x in self.nodes if x.machine != machine or x.user != user]
        self.nodes = new_nodes

        new_transitions = [x for x in self.transitions if (x.entry_node.machine != machine or x.entry_node.user != user)
                           and (x.exit_node.machine != machine or x.exit_node.user != user)]

        self.transitions = new_transitions

    def remove_transition_from_scenario(self, transition: "Transition"):
        """Removes a transition from the scenario based on a string representation.

        Args:
            transition: a transition
        """
        new_transitions = [x for x in self.transitions if x != transition]
        self.transitions = new_transitions

    def get_new_node_id(self) -> int:
        """Returns the lowest node_id that hasn't been used already.

        """
        already_used_ids = []
        for node in self.nodes:
            already_used_ids.append(node.node_id)
        if len(already_used_ids) == 0:
            return 0
        return max(already_used_ids) + 1

    def add_transition_to_scenario(self, entry_node: str, exit_node: str, technique: str, required: str,
                                   rewarded: str) -> None:
        """Adds a transition to the scenario. Inputs is described as strings and converted into objects.

        Args:
            entry_node: the entry node, written as "User, Machine"
            exit_node: the exit node, written as "User, Machine"
            technique: the associated technique, written as "TXXXX: Technique name"
            required: the required secrets, written as secret1, secret2, secret3... None for no secret.
            rewarded: the rewarded secrets, written as secret1, secret2, secret3... None for no secret.
        """

        def get_node_from_string(node_txt):
            machine = node_txt.split(", ")[0]
            user = node_txt.split(", ")[1]
            for node in self.nodes:
                if node.user == user and node.machine == machine:
                    return node
            raise ValueError("Node not found for " + node_txt)

        entry_node = get_node_from_string(entry_node)
        exit_node = get_node_from_string(exit_node)
        technique = technique.split(":")[0]

        if required == "None" and rewarded == "None":
            transition = Transition(entry_node, exit_node, technique)
        elif required == "None":
            transition = Transition(entry_node, exit_node, technique, rewards=rewarded.split(", "))
        elif rewarded == "None":
            transition = Transition(entry_node, exit_node, technique, requires=required.split(", "))
        else:
            transition = Transition(entry_node, exit_node, technique, required.split(", "), rewarded.split(", "))
        self.transitions.append(transition)

    @staticmethod
    def empty_scenario() -> "SystemGraph":
        """Creates an empty SystemGraph object object.
        """

        return SystemGraph([], [], [], [])


class Node:
    """A class used to represent an attack position.

    Attributes:
        machine (str): The name of the machine corresponding to the attack position.
        user (str): The name of the user corresponding to the attack position.
        node_id (int): A unique node identifier, generated automatically for every node created.
    """
    newid = itertools.count()

    def __init__(self, machine: str, user: str, node_id: int = -1) -> None:
        self.machine = machine.lower()
        self.user = user.lower()
        if node_id == -1:
            self.node_id = next(self.newid)
        else:
            self.node_id = node_id

    def __repr__(self) -> str:
        return str(self.machine) + ", " + str(self.user)

    def __eq__(self, other: "Node") -> bool:
        return self.machine == other.machine and self.user == other.user and self.node_id == other.node_id


class Transition:
    """A class used to represent an transition on the technical level.

    Attributes:
        entry_node (Node): The attack position the transition is initiated from.
        exit_node (Node) : The attack position the transition leads to (can be the same as entry).
        technique (str) : An ATT&CK technique, represented by its technique number (such as T1021).
        procedure (str) : A procedure instantiating the technique.
        requires (list[str]) :  A list of secrets required for the transition, can be empty.
        rewards (list[str]) : A list of secrets rewarded by the transition, can be empty.
        transition_id (int): A unique transition identifier, generated automatically for every transition.
    """
    newid = itertools.count()

    def __init__(self, entry_node: Node, exit_node: Node, technique: str, requires: list[str] = [],
                 rewards: list[str] = [], procedure: str = None) -> None:
        self.entry_node = entry_node
        self.exit_node = exit_node
        self.technique = technique
        self.procedure = procedure
        self.requires = requires
        self.rewards = rewards
        self.transition_id = next(self.newid)

    def __repr__(self) -> str:
        return "T([" + str(self.entry_node) + "] , [" + str(self.exit_node) + "])"

    def __eq__(self, other: "Transition") -> bool:
        return self.entry_node == other.entry_node and self.exit_node == other.exit_node \
            and self.technique == other.technique and self.transition_id == other.transition_id


class AttackState:
    """A class used to represent the state of an attacker in the scenario.

    Attributes:
        nodes (list[Node]): The list of attack positions controlled by the attacker.
        transitions (list[Transition]) : The list of transitions that were taken by the attacker.
        secrets (list[str]) : The list of secrets that were acquired by the attacker.
    """

    def __init__(self, nodes: list[Node], transitions: list["Transition"], secrets: list[str]) -> None:
        self.nodes = nodes
        self.transitions = transitions
        self.secrets = secrets

    def __repr__(self) -> str:
        return "Nodes : " + str(self.nodes) + " , Transitions : " \
            + str(self.transitions) + " , Secrets : " + str(self.secrets)

    def __eq__(self, other: "AttackState") -> bool:
        if self.nodes == other.nodes and self.transitions == other.transitions and self.secrets == other.secrets:
            return True
        return False

    @staticmethod
    def default_attack_state(system_graph: "SystemGraph") -> "AttackState":
        """ Returns an attack state for the given system_graph where the attacker controls all starting nodes and
        has no secrets.

        Args:
            system_graph (SystemGraph): The system graph object to generate an attack state for.

        Returns:
            The corresponding AttackState object

        """
        return AttackState(system_graph.starting_nodes, [], [])

    def is_winning(self, system_graph: "SystemGraph") -> bool:
        """Checks if the AttackState corresponds to a winning AttackState

        Args:
            system_graph (SystemGraph): the scenario being played

        Returns:
            True if the scenario is won, False otherwise.

        """
        for node in self.nodes:
            if node in system_graph.winning_nodes:
                return True
        return False

    def take_transition(self, transition: "Transition") -> None:
        """Updates the AttackState by taking a transition, and acquiring new attack positions and secrets as a result.

               Args:
                   transition (Transition): The transition to take
               Raises:
                    InvalidTransitionError: The attacker is not actually able to take this transition.

               """

        def add_no_duplicate(list_to_check: list, element: object) -> list:
            """Adds an element to a list only if it's not contained in the list already.

            Args:
                list_to_check: the list to check.
                element: the element to maybe add.

            Returns:
                The list completed.
            """
            if element in list_to_check:
                return list_to_check
            else:
                list_to_check.append(element)
                return list_to_check

        if not self.is_transition_valid(transition):
            raise InvalidTransitionError
        self.nodes = add_no_duplicate(self.nodes, transition.exit_node)
        self.transitions = add_no_duplicate(self.transitions, transition)
        for secret in transition.rewards:
            self.secrets = add_no_duplicate(self.secrets, secret)

    def is_transition_valid(self, transition: "Transition") -> bool:
        """ Checks if an attacker may effectively take a given transition.
        This is done by checking that he controls the starting node and knows the required secrets.

        Args:
            transition (Transition): The transition to check.

        Returns:
            True if the transition is possible to take, False otherwise.

        """
        if transition.entry_node not in self.nodes:
            return False
        for secret in transition.requires:
            if secret not in self.secrets:
                return False
        return True

    def take_random_action(self, system_graph: "SystemGraph") -> None:
        """ Makes the attacker take a random transition among all the ones he has available.

        Args:
            system_graph(SystemGraph): The scenario actions are taken from.

        Raises:
            NoValidTransitionError: The attacker has no valid moves left.

        """
        # takes a legal action for the attacker at random
        legal_transitions = []
        # get all possible transitions
        for t in system_graph.transitions:
            if self.is_transition_valid(t) and t not in self.transitions:
                legal_transitions.append(t)
        if not legal_transitions:
            raise NoValidTransitionsError
        chosen_attack = rd.choice(legal_transitions)
        self.take_transition(chosen_attack)


class AttackPath:
    """A class used to represent the journey of an attacker in the scenario.


        Attributes:
            attack_states: The list of all attack states representing the attack path.
            transitions: The list of all transitions that were taken to lead to these attack states.

    """

    def __init__(self, attack_states: list["AttackState"], transitions: list["Transition"]) -> None:
        self.attack_states = attack_states
        self.transitions = transitions

    @staticmethod
    def generate_random_attack_path(system_graph: "SystemGraph", number_of_actions: int) -> "AttackPath":
        """Generates a random attack path for an attacker with a randomized starting position taking n transitions.
        Only transitions that lead to a different attack state will be taken. Therefore, if number_of_actions is too big
        this algorithm may end early.
        The attacker will take a valid transition at random at every step.

        Args:
            system_graph: The scenario to walk.
            number_of_actions: The number of transitions the attacker will take at random.

        Returns:

        """
        # Initializes a random starting attack path
        # Get a random > 0 number of starting nodes
        n_starting_nodes = rd.randint(1, len(system_graph.starting_nodes))
        starting_nodes = rd.sample(system_graph.starting_nodes, n_starting_nodes)
        starting_attack_state = AttackState(starting_nodes, [], [])
        attack_path = AttackPath([starting_attack_state], [])
        # Takes number_of_actions transitions at random. only takes transitions that lead to a different state.
        for i in range(number_of_actions):
            legal_transitions = []
            # Get the current attack state
            current_attack_state = attack_path.attack_states[-1]
            for transition in system_graph.transitions:
                if current_attack_state.is_transition_valid(transition):
                    legal_transitions.append(transition)
            if len(legal_transitions) == 0:
                log.info(f"Random attack path generation ended early - no more valid transitions left.")
            for transition in legal_transitions:
                new_attack_state = copy.deepcopy(current_attack_state)
                new_attack_state.take_transition(transition)
                if new_attack_state.nodes != current_attack_state.nodes or new_attack_state.secrets != \
                        current_attack_state.secrets:
                    # We add this new state and the new transition to the attack path
                    attack_path.attack_states.append(new_attack_state)
                    attack_path.transitions.append(transition)
        return attack_path

    def check_if_valid(self, system_graph: "SystemGraph") -> bool:
        """Checks whether the attack path is valid for a given scenario.
        This is done by checking that each attack state does result from taking each transition one by one.

        Args:
            system_graph (SystemGraph): the scenario to check against

        Returns:
            True if the path is valid, False otherwise

        """
        # Check that all first nodes of the path are in the starting nodes of the graph
        for node in self.attack_states[0].nodes:
            if node not in system_graph.starting_nodes:
                return False
        # Check that all transitions were valid
        for i in range(len(self.transitions)):
            current_transition = self.transitions[i]
            current_attack_state = self.attack_states[i]
            new_attack_state = copy.deepcopy(current_attack_state)
            new_attack_state.take_transition(current_transition)
            if new_attack_state != self.attack_states[i + 1]:
                return False
        return True

    def check_if_winning(self, system_graph: "SystemGraph") -> bool:
        """Checks if an attack path corresponds to a winning attack path for a given scenario.

        Args:
            system_graph (SystemGraph): The scenario to check against.

        Returns:
            True if the attack path is winning, False otherwise.
        """
        final_state = self.attack_states[-1]
        for node in final_state.nodes:
            if node in system_graph.winning_nodes:
                return True
        return False


def export_scenario_as_json(sys_graph: "SystemGraph", output_file_name: str) -> None:
    """Exports a SystemGraph object as a json file, which may be imported in the future using import_scenario_as_json.

    Args:
        sys_graph (SystemGraph): The scenario to export.
        output_file_name (str): The name of the output json file. Will be exported in the ../json directory.

     """
    final_dic = {"Nodes": [], "Transitions": [], "Starting": [], "Winning": []}
    for node in sys_graph.nodes:
        final_dic["Nodes"].append({"Machine": node.machine, "User": node.user, "Node_id": node.node_id})
    for transition in sys_graph.transitions:
        final_dic["Transitions"].append({"Entry": transition.entry_node.node_id,
                                         "Exit": transition.exit_node.node_id,
                                         "Technique": transition.technique,
                                         "Procedure": transition.procedure,
                                         "Requires": transition.requires,
                                         "Rewards": transition.rewards})
    for node in sys_graph.starting_nodes:
        final_dic["Starting"].append(({"Machine": node.machine, "User": node.user, "Node_id": node.node_id}))
    for node in sys_graph.winning_nodes:
        final_dic["Winning"].append(({"Machine": node.machine, "User": node.user, "Node_id": node.node_id}))
    with open(output_file_name, 'w+') as outfile:
        json.dump(final_dic, outfile)


def import_scenario_from_json(json_file_path: str) -> "SystemGraph":
    """Imports a json file into a SystemGraph object.
    TODO: check if this still works.
    Args:
        json_file_path (str): The path of the json file to import.
    Returns:
        A SystemGraph object, corresponding to the information stored in json_file_path.
    Raises:
        FileNotFoundError: The inputted file path is not a json file


     """
    with open(json_file_path) as json_file:
        scenario_dictionary = json.load(json_file)
    final_nodes = []

    def get_node_from_id(node_list: list["Node"], node_id: str) -> "Node":
        """Looks for a node of a given id inside the list.
        # TODO: check if this still works.
        Args:
            node_list: The list of nodes.
            node_id: The id of the node.

        Returns:
            The indice of the node if present in the list, -1 otherwise.
        """
        for node_from_list in node_list:
            if node_from_list.node_id == node_id:
                return node_from_list
        raise ValueError

    for node in scenario_dictionary["Nodes"]:
        final_nodes.append(Node(node["Machine"], node["User"], node["Node_id"]))
    final_transitions = []
    for transition in scenario_dictionary["Transitions"]:
        final_transitions.append(Transition(get_node_from_id(final_nodes, transition["Entry"]),
                                            get_node_from_id(final_nodes, transition["Exit"]),
                                            transition["Technique"],
                                            transition["Requires"], transition["Rewards"], transition["Procedure"]
                                            ))
    final_starting = []
    for starting in scenario_dictionary["Starting"]:
        final_starting.append(get_node_from_id(final_nodes, starting["Node_id"]))
    final_winning = []
    for winning in scenario_dictionary["Winning"]:
        final_winning.append(get_node_from_id(final_nodes, winning["Node_id"]))
    scenario_object = SystemGraph(final_nodes, final_starting, final_winning, final_transitions)
    return scenario_object


def get_scenario_difficulty(scenario: SystemGraph) -> int:
    """Gets the difficulty of a given scenario by summing the difficulty score of all its transitions that already
    have transitions attached to them.

    Args:
        scenario: the scenario to check.

    Returns:
        The difficulty score.

    """
    final_difficulty = 0
    for transition in scenario.transitions:
        if transition.procedure:
            proc = chouchen.constraints.ArchitecturalConstraints.get_dictionary_from_procedure_name(transition)
            final_difficulty += proc["Difficulty"]
    return final_difficulty
