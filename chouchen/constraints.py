# -*- coding: utf-8 -*-
"""Architectural constraint definitions.

This module codifies what an architectural constraint is using the ArchitecturalConstraint class.
An ArchitecturalConstraint object is composed of 4 constraints:

    - OSConstraint, a class responsible for operating system conditions applying to procedures, including its version and type.
    - AccountConstraint, the same for accounts, including its name, group, privilege, credentials and services.
    - SoftwareConstraint, the same for software, including its version and type.
    - FileConstraint, the same for files, including its path, permissions and content.

This module is also responsible for SecretPrecondition objects, which make sure that procedures are picked accordingly
to the technical scenario.

Once procedures are loaded from their json files, their corresponding ArchitecturalConstraint objects are generated.
They are then combined according to our refinement backtracking algorithm.
"""

import json
import logging
import os.path
import random as rd
from os import listdir
from os.path import isfile, join

import chouchen.file_generation
import chouchen.secret_generation as secg
import chouchen.subconstraints as subconstraints
import chouchen.system_graph as sg

log = logging.getLogger(__name__)


class OSConstraints:
    """A class to represent OSConstraints and check their mutual compatibilities.
    OS constraints contain a type (Windows, Ubuntu...) and a version (<=20.04, ==16.04...).

    Current limitations:
    - A procedure may only have a single OS constraint. If you want to have a procedure that can work on Ubuntu
    AND Debian, you will have to create 2 different procedures.
    - If you want to specify specific versions (such as 16.04 OR 20.04), you will have to write it as an interval
    instead (>=16.04, <=20.04).

    Attributes:
        os_type (str): The type of the OS.
        os_version str: The acceptable OS versions.

    Examples:
        >>> ubuntu_1604 = OSConstraints("canonical:ubuntu_linux", "==16.04")
        >>> debian_10 = OSConstraints("debian:debian_linux", "==10.0")

    """

    def __init__(self, os_type: str, os_version: str):
        self.os_type = subconstraints.OSType(os_type)
        self.version = subconstraints.OSVersion(os_version)

    def __repr__(self) -> str:
        return f"""{self.os_type}, {self.version}"""

    def is_compatible(self, other: "OSConstraints") -> bool:
        """ Checks if two OS constraints are compatible with each other, i.e that they may coexist on the same
        architecture.

        2 os constraints are compatible if:
        - They represent the same type of OS and have compatible versions. For instance Ubuntu <= 16.04
        and Ubuntu == 16.04.
        - Their OS type are compatible and they have compatible versions. For instance Linux * and Ubuntu == 16.04.

        Args:
            other (OSConstraints): The constraint to compare against.

        Returns:
            True if the 2 constraints are compatible, False otherwise.

        Examples:
            >>> ubuntu_1604 = OSConstraints("canonical:ubuntu_linux", "==16.04")
            >>> ubuntu_2004 = OSConstraints("canonical:ubuntu_linux", "==20.04")
            >>> ubuntu_1604.is_compatible(ubuntu_2004)
            False
        """
        return self.os_type.is_compatible(other.os_type) and self.version.is_compatible(other.version)

    def to_dic(self) -> dict:
        """Convert the OSConstraints object into a format compatible with our final configuration file.
        Returns:
            a dictionnary representing the constraint
        """
        return {"Type": self.os_type.to_config_format(), "Version": self.version.to_config_format()}

    @staticmethod
    def fuse_duplicates(first: "OSConstraints", other: "OSConstraints") \
            -> "OSConstraints":
        """Fuses 2 OS entries that are compatible with each other
        This is done by getting the smallest constraint for each of Type and Version
        Args:
            first (OSConstraints): the first contraint to be fused
            other (OSConstraints): the constraint to fuse with.

        Returns: The fused entry
        """

        new_constraint = OSConstraints.default_os_constraint()
        new_constraint.os_type = first.os_type.fuse(other.os_type)
        new_constraint.version = first.version.fuse(other.version)
        return new_constraint

    @staticmethod
    def default_os_constraint() -> "OSConstraints":
        """Creates a wildcard OS constraint, representing a lack of constraints.
        Used in fuse_duplicates.

        Returns:
            a wildcard OS constraint, compatible with everything.
        """
        return OSConstraints("*", "*")


class AccountConstraints:
    """A class to represent account constraints and their compatibilities.
    Account constraints consist of a name ("Alice", "root" ...), group ("Bob", "root" ...), permissions ("USER" or
    "SUPERUSER") and credentials ("WEAK_RANDOM_PASSWORD", ...).
    TODO: list of acceptable credential types.
    Attributes:
        name (subconstraints.AccountName): the name constraint.
        group (subconstraints.AccountGroup): the group constraint.
        privilege (str): the permission constraint.
        credentials (dict[str]): the credentials constraint.
        services (list[str]): the service constraint
        assigned_secrets (list[str]): the list of assigned secrets

       Examples:
            >>> credential_dic = {"Credential Type": "RANDOM_WEAK_PASSWORD", "Requires Secret": False}
            >>> alice_weak_password = AccountConstraints("alice", group = "alice", credentials=credential_dic)



    """

    def __init__(self, name: str, group: str = subconstraints.AccountGroup.Wildcard,
                 privilege: str = subconstraints.AccountPrivilege.Wildcard,
                 credentials: dict[str] = subconstraints.AccountCredentials.wildcard_dic(),
                 services: list[str] = None) -> None:
        self.name = subconstraints.AccountName(name)
        self.group = subconstraints.AccountGroup(group)
        self.privilege = subconstraints.AccountPrivilege(privilege)
        requires_secret = credentials["Requires Secret"]
        if requires_secret:
            self.credentials = subconstraints.AccountCredentials(credentials["Credential Type"],
                                                                 credentials["Requires Secret"],
                                                                 credentials["Secret Type"],
                                                                 credentials["Secret Amount"])
        else:
            self.credentials = subconstraints.AccountCredentials(credentials["Credential Type"],
                                                                 credentials["Requires Secret"])
        if not services:
            self.services = subconstraints.AccountServices([])
        else:
            self.services = subconstraints.AccountServices(services)
        self.assigned_secrets = []

    def __repr__(self) -> str:
        return f"""{self.name}, {self.group}, {self.privilege}, {self.credentials}, {self.services}"""

    def set_assigned_secrets(self, lsecrets: list[str]):
        """ Sets the list of secrets assignated to the constraint to the given value.

        Args:
            lsecrets (list[str]): The list of secrets to set.
        """
        self.assigned_secrets = lsecrets

    def is_compatible(self, other: "AccountConstraints") -> bool:
        """ Checks if 2 account constraints are compatible with each other, i.e that they may coexist on the same
        architecture.

        2 account constraints are comaptible if:
        - They have different names.
        - They have compatible group, permissions and credentials subconstraints.

        Args:
            other (AccountConstraints): the constraint to check against.

        Returns:
            True if the 2 constraints are compatible, False otherwise.
        """
        return self.name.is_compatible(other.name) or ((self.group.is_compatible(other.group)) and
                                                       (self.privilege.is_compatible(other.privilege))
                                                       and (self.credentials.is_compatible(other.credentials)) and
                                                       self.services.is_compatible(other.services))

    def to_dic(self, secret_dictionary: dict) -> dict:
        """Convert the AccountConstraints object into a format compatible with our final configuration file.
        This requires the secret dictionary in argument in order to generate the credentials.

        Returns:
            a dictionnary representing the constraint
        """
        return {"Name": str(self.name), "Group": str(self.group), "Privilege": str(self.privilege),
                "Services": self.services.service_list,
                "Credentials": self.credentials.to_config_format(self.assigned_secrets, secret_dictionary)}

    @staticmethod
    def fuse_duplicates(first: "AccountConstraints", other: "AccountConstraints") -> "AccountConstraints":
        """Fuses 2 entries with the same user name.
        This is done by getting the smallest constraint for each of Group, Privilege and Credentials (usually the one
        that is not a wildcard).
        Args:
            first (AccountConstraints): the first contraint to be fused
            other (AccountConstraints): the constraint to fuse with.

        Returns: The fused entry
        """

        assert first.name == other.name
        new_constraint = AccountConstraints(str(first.name), first.group.fuse(other.group).name,
                                            str(first.privilege.fuse(other.privilege)),
                                            services=first.services.fuse(other.services).service_list)
        new_constraint.credentials = first.credentials.fuse(other.credentials)
        new_constraint.assigned_secrets = first.assigned_secrets + other.assigned_secrets
        return new_constraint


class SoftwareConstraints:
    """ A class to represent software constraints and their compatibilities.
    Software constraints contain a type (ssh, windows_rdp ...), a version (==1.8.2 ...) and optionally a port (22 ...).

    Attributes:
        software_type (str): the software type.
        version (list[(int, str)])): the software versions.
        port (int): the port used by the software. Set to -1 if not initialized.

    Examples:
        >>> sudo_182 = SoftwareConstraints("sudo_project:sudo", "==1.8.27", -1)
    """

    def __init__(self, software_type: str, version: str, port: int = -1):
        self.software_type = subconstraints.SoftwareType(software_type)
        self.version = subconstraints.SoftwareVersion(version)
        self.port = subconstraints.SoftwarePort(port)

    def __repr__(self) -> str:
        return f"""{self.software_type}, {self.version}, {self.port}"""

    def is_compatible(self, other: "SoftwareConstraints") -> bool:
        """ Checks if two software constraints are compatible with each other, i.e that they may coexist on the same
        architecture.

        2 software constraints are compatible if:
        - They represent the same type of software, have compatible versions and the same port.
        For instance sudo <= 1.9.5 and sudo >= 1.9.0, both with no ports.
        - They represent 2 different types of software and don't use the same port. For instance ssh (port 22) and
        sudo (no port)

        Args:
            other (SoftwareConstraints): The constraint to check against.

        Returns:
            True if the 2 constraints are compatible, False otherwise.

        Examples:
            >>>

        """
        return (not (self.software_type == other.software_type) and not (self.port.is_compatible(other.port))) or \
            ((self.software_type == other.software_type) and (self.version.is_compatible(other.version))
             and (self.port == other.port))

    def to_dic(self) -> dict:
        """Convert the SoftwareConstraints object into a format compatible with our final configuration file.
        This is done by converting the type, version and port to their respective configuration format, and outputting
        a dictionary containing everything.

        Returns:
            a dictionnary representing the constraint
        """
        return {"Type": self.software_type.to_config_format(),
                "Version": self.version.to_config_format(),
                "Port": self.port.to_config_format()}


class FileConstraints:
    """A class to represent file constraints and their compatibilities.
    File constraints consist of a path constraint (where the file will be stored on the machine), permission constraints
    (written in the UNIX numbered format) and content constraints (hardcoded in subconstraints.FileContent).
    During the refinement process this will also remember which secret name in the scenario
    (ie the associated transition) is needed for the file construction (if there is any).

    Attributes:
        path (str): the path of the file.
        perm (subconstraints.FilePermission): the permissions required for the file.
        content (dict): a dictionary containing information related to the content of the file.
        assigned_secrets (list[str]): the list of secrets associated to the file. Can be empty.
        machine_name (str): the name of the associated machine.
        user_name (str): the name of the associated user.
    """

    def __init__(self, path: str, perm: str, user: str, group: str, content: dict) -> None:
        self.path = subconstraints.FilePath(path)
        self.perm = subconstraints.FilePermission(perm, user, group)
        requires_secret = content["Requires Secret"]
        if requires_secret:
            self.content = subconstraints.FileContent(content["Content Type"], content["Requires Secret"],
                                                      content["Secret Type"], content["Secret Amount"])
        else:
            self.content = subconstraints.FileContent(content["Content Type"], content["Requires Secret"])
        self.assigned_secrets = []
        self.machine_name = None
        self.user_name = None

    def __repr__(self) -> str:
        return f"""{self.path}, {self.perm}, {self.content}"""

    def set_assigned_secrets(self, lsecrets: list[str]):
        """ Sets the list of associated secrets to this procedure.

        Args:
            lsecrets (list[str]): the list of assignated secrets.
        """
        self.assigned_secrets = lsecrets

    def set_machine_name(self, machine_name: str):
        """ Sets the machine_name attribute to a new value.
        TODO: remember what this is used for.
        Args:
            machine_name (str): the machine name to assign.
        """
        self.machine_name = machine_name

    def set_user_name(self, user_name: str):
        """ Sets the user_name attribute to a new value.
        TODO: remember what this is used for.
        Args:
            user_name (str): the username to assign.
        """
        self.user_name = user_name

    def is_compatible(self, other: "FileConstraints") -> bool:
        """ Checks if two file constraints objects are compatible.

        2 file constraints are compatible if:
        - They link to different file paths.
        - They link to the same file path but have compatible permissions and content constraints.
        Args:
            other(FileConstraints): the file constraint to check against.

        Returns:
            True if the two constraints are compatible, False otherwise.
        """
        return (self.path != other.path) or ((self.perm.is_compatible(other.perm))
                                             and (self.content.is_compatible(other.content)))

    def to_dic(self, secret_dictionary: dict, already_generated: bool = False) -> dict:
        """Convert the FileConstraints object into a format compatible with our final configuration file
        NOTE: This will also generate the related file (which may generate secrets itself!).
        This function will actually be called twice. We do not want to regenerate files we already generated before,
        hence the already_generated parameter.
        Returns:
            a dictionnary representing the constraint

        Args:
            secret_dictionary (dict): the secret dictionary. Necessary to generate credentials inside files.
            already_generated (bool): indicates whether files will need to be regenerated.

        Returns:
            A dictionary representing the constraint.
        """
        source, modification = chouchen.file_generation.generate_file(self, secret_dictionary,
                                                                      self.machine_name, self.user_name,
                                                                      already_generated)

        return {"Destination": str(self.path),
                "Source": source,
                "Modification": modification,
                "Owner": str(self.perm.user),
                "Group": str(self.perm.group),
                "Permissions": str(self.perm.perm)}


class CombiningIncompatibleConstraintsError(Exception):
    """Exception raised for errors when trying to combine ArchitecturalConstraints objects

    Attributes:
        constraint_1 (ArchitecturalConstraints): The first constraint.
        constraint_2 (ArchitecturalConstraints): The second constraint.
        message (str): explanation of the error.
    """

    def __init__(self, constraint_1: "ArchitecturalConstraints",
                 constraint_2: "ArchitecturalConstraints", message: object = "Incompatible constraints"):
        self.constraint_1 = constraint_1
        self.constraint_2 = constraint_2
        super().__init__(message)


class ArchitecturalConstraints:
    """A class to represent procedure constraints and their compatibilities. Procedure constraints consist of
    OS constraints, Account constraints, Software constraints and File constraints.
    A instance of this class may be created by providing it a procedure stored in a dictionary format.

   Attributes:
       os_constraints (OSConstraints): the list of OS constraints.
       account_constraints (AccountConstraints): the list of Account constraints.
       software_constraints (SoftwareConstraints): the list of Software constraints.
       file_constraints (FileConstraints): the list of File constraints.
       assignated_secrets (list[str]): the list of all secrets related to the transition associated to this procedure.

       """

    def __init__(self, proc_dic: dict, transition: "sg.Transition",
                 machine_name: str = None, user_name: str = None) -> None:
        self.os_constraints = []
        self.account_constraints = []
        self.software_constraints = []
        self.file_constraints = []
        if transition:
            self.assignated_secrets = transition.requires + transition.rewards
            self.required_assignated_secrets = transition.requires
            self.rewarded_assignated_secrets = transition.rewards
        else:
            self.assignated_secrets = []
        for os in proc_dic["OS"]:
            self.os_constraints.append(OSConstraints(os["Type"], os["Version"]))
        for account in proc_dic["Account"]:
            if account["Name"] == "USER_NAME":
                account["Name"] = user_name
            if "Services" in account:
                self.account_constraints.append(
                    AccountConstraints(account["Name"], account["Group"], account["Privilege"],
                                       account["Credentials"], account["Services"]))
            else:
                self.account_constraints.append(
                    AccountConstraints(account["Name"], account["Group"], account["Privilege"],
                                       account["Credentials"]))
        for software in proc_dic["Software"]:
            if "Port" in software:
                software_constraint = SoftwareConstraints(software["Type"], software["Version"], software["Port"])
            else:
                software_constraint = SoftwareConstraints(software["Type"], software["Version"])
            self.software_constraints.append(software_constraint)

        for file in proc_dic["Files"]:
            perm = file["Permissions"]["Perm"]
            user = file["Permissions"]["User"]
            group = file["Permissions"]["Group"]
            self.file_constraints.append(FileConstraints(file["Path"],
                                                         perm, user, group, file["Content"]))
        for file_constraint in self.file_constraints:
            file_constraint.set_machine_name(machine_name)
            file_constraint.set_user_name(user_name)

    def __repr__(self) -> str:
        return f"""{self.os_constraints}, {self.account_constraints}, {self.software_constraints}, {self.file_constraints}"""

    def assign_secret(self, secret_list: list[str]):
        """Adds secrets to this architectural constraint.
        See README for more details on why this is done.

        Args:
            secret_list (list): the list of secrets to add.
        """
        self.assignated_secrets += secret_list

    def assign_secrets_to_subconstraints(self) -> None:
        """Assigns secrets to each account (and later file) constraints.
        This is called when the architectural constraint just got created and only contains information about a
        procedure's constraints.
        There should not be account AND file constraints requiring secrets in a single procedure.
        First this gives secrets to each account constraint that require specific numbers of them.
        Then it gives the leftover secrets to the constraints with a wildcard amount of them (at least 1 for each).
        TODO: This function is currently very janky and unpractical. Needs a rewrite.
        This should be rewritten from scratch eventually, the concept of giving secrets to subconstraints seems sound though.
        IMPORTANT: IN PROCEDURES, FILES REQUIRING SECRETS SHOULD BE WRITTEN BEFORE FILES REQUIRING REWARDS THE ORDER MATTERS
        Returns:

        """

        # randomly assigns n secrets to m constraints
        def random_list(m: int, n: int) -> list:
            """Assigns n elements to a list of size m.

            Args:
                m: An int. Can't be 0.
                n: An other int.

            Returns:
                A list of size m where the sum of all its elements is n.
            """
            assert m != 0
            arr = [0] * m
            for j in range(n):
                arr[rd.randint(1, n) % m] += 1
            return arr

        # Need to distribute the assignated secrets to each constraint
        # Check whether this is a file based constraint or an account based constraint
        # It can't be both otherwise everything explodes
        has_account_constraint_with_secret = False
        has_file_constraint_with_secret = False
        for x in self.account_constraints:
            if x.credentials.requires_secret:
                has_account_constraint_with_secret = True

        for x in self.file_constraints:
            if x.content.requires_secret:
                has_file_constraint_with_secret = True

        assert not (has_account_constraint_with_secret and has_file_constraint_with_secret)

        if not has_account_constraint_with_secret and not has_file_constraint_with_secret:
            return

        if has_account_constraint_with_secret:
            number_already_given = 0
            # First we iterate through all user constraints and check how many they need

            # Give ever constraint that requires a specific number of secrets their secrets
            # This is done by adding them
            wildcard_count = 0
            for i in range(len(self.account_constraints)):
                constraint = self.account_constraints[i]
                if constraint.credentials.requires_secret:
                    if constraint.credentials.secret_amount != subconstraints.AccountCredentials.Wildcard and constraint.credentials.secret_amount is not None:
                        self.account_constraints[i].set_assigned_secrets(
                            self.assignated_secrets[
                            number_already_given:number_already_given + int(constraint.credentials.secret_amount)])
                        number_already_given += 1

                    else:
                        wildcard_count += 1
            if number_already_given < len(self.assignated_secrets):  # if there's spare secrets to distribute
                repartition = random_list(wildcard_count, len(self.assignated_secrets) -
                                          number_already_given)
                nb_already_seen = 0
                for i in range(len(self.account_constraints)):
                    constraint = self.account_constraints[i]

                    if constraint.credentials.secret_amount == subconstraints.AccountCredentials.Wildcard:
                        self.account_constraints[i].set_assigned_secrets(
                            self.assignated_secrets[number_already_given:number_already_given +
                                                                         repartition[nb_already_seen]])

                        number_already_given += 1
                        nb_already_seen += 1

        else:  # UGLIEST COPY PASTE i'm sorry
            # First we iterate through all user constraints and check how many they need
            # Give ever constraint that requires a specific number of secrets their secrets
            # This is done by adding them
            number_already_given = 0
            wildcard_count = 0
            for i in range(len(self.file_constraints)):
                constraint = self.file_constraints[i]
                if constraint.content.requires_secret:
                    if constraint.content.secret_amount != subconstraints.FileContent.Wildcard \
                            and constraint.content.secret_amount is not None:
                        self.file_constraints[i].set_assigned_secrets(
                            self.assignated_secrets[
                            number_already_given:number_already_given + int(constraint.content.secret_amount)])
                        number_already_given += 1
                    else:
                        wildcard_count += 1
            if number_already_given < len(self.assignated_secrets):  # if there's spare secrets to distribute
                repartition = random_list(wildcard_count, len(self.assignated_secrets) -
                                          number_already_given)
                nb_already_seen = 0
                for i in range(len(self.file_constraints)):
                    constraint = self.file_constraints[i]

                    if constraint.content.secret_amount == subconstraints.FileContent.Wildcard:
                        self.file_constraints[i].set_assigned_secrets(self.assignated_secrets[
                                                                      number_already_given:number_already_given +
                                                                                           repartition[
                                                                                               nb_already_seen]])
                        number_already_given += 1
                        nb_already_seen += 1

    def edit_special_cases(self, scenario: "sg.SystemGraph"):
        """Edits a few static values to their corresponding real value.
        For now this is only used for the /etc/shadow procedure, which requires setting the account constraint
        to a specific value which can only be found somewhere else in the scenario

        Args:
            scenario (sg.SystemGraph): the scenario

        """

        for account_constraint in self.account_constraints:
            if account_constraint.name.name == \
                    subconstraints.AccountName.Take_Value_From_Transition_Requiring_Same_Secret:
                # Find the transition in the graph which requires the secret that has been assignated to this procedure
                for transition in scenario.transitions:
                    # We found the transition
                    if account_constraint.assigned_secrets == transition.requires:
                        account_constraint.name.name = transition.exit_node.user

    def to_dic(self, name: str, secret_dictionary: dict) -> dict:
        """Converts all the architectural constraints into a dictionary which will be added to the final json output
        Args:
            name (str): the name of the machine corresponding to these constraints
            secret_dictionary (dict): the secret dictionary
        Returns:
            A dictionary containing all information necessary to deploy this specific machine.
        """

        os_constraints_dic_list = [x.to_dic() for x in self.os_constraints]

        software_constraints_dic_list = [x.to_dic() for x in self.software_constraints]

        users_constraints_dic_list = [account_constraint.to_dic(secret_dictionary)
                                      for account_constraint in self.account_constraints]
        file_constraints_dic_list = [file_constraint.to_dic(secret_dictionary, True)
                                     for file_constraint in self.file_constraints]

        output_dic = {"Name": name, "OS": os_constraints_dic_list, "Software": software_constraints_dic_list,
                      "Users": users_constraints_dic_list, "Files": file_constraints_dic_list}
        return output_dic

    def generate_credentials_and_files(self, secret_dictionary: dict) -> list[dict]:
        """Goes through all the file and account constraints, in particular their FileContent et AccountCredentials
        subconstraints, and generates the appropriate secrets based on the secret names contained in
        self.assignated_secrets.
        For now this only works if the constraint has either an account or a file constraint requiring secrets
        (but not both). Reparting secrets when that is not the case is surprisingly annoying, but this should cover
        most cases. This function thus will give all secrets it has been assignated to the subconstraint it has.
        Then based on the value of secret_value, secret_type, credentials/content and the secret dictionary, this
        will call the secret and file generating module.


        Args:
            secret_dictionary (dict): the secret dictionary. Will be updated by this function.

        Returns:
            A list of all configuration dictionaries related to the generated secrets and files.
        """
        dic_list = []
        for account_constraint in self.account_constraints:
            dic_list.append(account_constraint.to_dic(secret_dictionary))

        for file_constraint in self.file_constraints:
            dic_list.append(file_constraint.to_dic(secret_dictionary))
        return dic_list

    def is_empty(self) -> bool:
        """ Checks whether an architectural constraint is empty by checking the length of all its constraints.

        Returns:
            True if the constraint is empty, false otherwise.
        """
        return len(self.os_constraints) == 0 and len(self.file_constraints) == 0 and \
            len(self.account_constraints) == 0 and len(self.software_constraints) == 0

    def is_compatible(self, other: "ArchitecturalConstraints") -> bool:
        """
        2 architectural constraints are compatible if all their subconstraints are compatible with each other.
        Args:
            other(ArchitecturalConstraints): the architectural constraint to check against.

        Returns:
            True if the two constraints are compatible, False otherwise.
        """
        for os_constraint in self.os_constraints:
            for other_os_constraint in other.os_constraints:
                if not os_constraint.is_compatible(other_os_constraint):
                    log.debug(
                        f""" OS incompatibility between "{os_constraint} and {other_os_constraint}""")
                    return False

        for software_constraint in self.software_constraints:
            for other_software_constraint in other.software_constraints:
                if not software_constraint.is_compatible(other_software_constraint):
                    log.debug(
                        f"""Software incompatibility between"{software_constraint} and {other_software_constraint}""")
                    return False

        for file_constraint in self.file_constraints:
            for other_file_constraint in other.file_constraints:
                if not file_constraint.is_compatible(other_file_constraint):
                    log.debug(
                        f""" File incompatibility between "{file_constraint} and {other_file_constraint}""")
                    return False

        for account_constraint in self.account_constraints:
            for other_account_constraint in other.account_constraints:
                if not account_constraint.is_compatible(other_account_constraint):
                    log.debug(
                        f"""Account incompatibility between "{account_constraint} and {other_account_constraint}""")
                    return False

        return True

    def is_user_in_constraint(self, user_name: str) -> bool:
        """Checks whether a given username has an entry in the architectural constraints already.
        This is useful for initializing the refinement process.

        Args:
            user_name: The name to look for

        Returns: True if the name already exists, False otherwise.

        """
        for account_constraint in self.account_constraints:
            if account_constraint.name.name == user_name:
                return True
        return False

    @staticmethod
    def get_dictionary_from_procedure_name(transition: "sg.Transition") -> dict:
        """ Gets the dictionary corresponding to a procedure constraint from its name.
        Raises an error if the name can't be found in the ./json/fused_procedures.json file.

        Args:
            transition: the related transition (contains a technique name).

        Returns:
            a dictionary containing the related procedure constraints.
        """
        JSON_FUSED_FILE = "./json/fused_procedures.json"
        with open(JSON_FUSED_FILE, "r") as inFile:
            proc_dic = json.load(inFile)
            proc_list = proc_dic[transition.technique]
        for proc in proc_list:
            if proc["Name"] == transition.procedure:
                return proc
        raise ValueError(f"""Procedure {transition.procedure}could not be found - check your spelling""")

    @staticmethod
    def get_list_from_procedure_pattern(transition: "sg.Transition") -> list[dict]:
        """ Gets the list of all dictionaries corresponding to a procedure pattern from its name.
            Raises an error if noi pattern matching can't be found in the ./json/fused_procedures.json file.

            Args:
                transition: the related transition (contains a technique name).

            Returns:
                a list of dictionaries containing the related procedures constraints.
                """
        JSON_FUSED_FILE = "./json/fused_procedures.json"
        result = []
        with open(JSON_FUSED_FILE, "r") as inFile:
            proc_dic = json.load(inFile)
            proc_list = proc_dic[transition.technique]
        for proc in proc_list:
            # pattern = re.compile(transition.procedure) IDK why I used to do that but it doesn't work
            # if pattern.match(proc["Name"]):
            # result.append(proc)
            if proc["Name"] == transition.procedure:
                result.append(proc)
        if len(result) > 0:
            return result
        raise ValueError(
            f"""Procedures matching "{transition.procedure}" (which should correspond to technique "{transition.technique}" could not be found.
This may be due to a lack of implementation of the given procedure in the appropriate json file, or a simple typo.""")

    @staticmethod
    def get_constraint_from_procedure_name(proc_name: str, transition: "sg.Transition",
                                           entry_or_exit: str) -> "ArchitecturalConstraints":
        """ Browses all procedure jsons (defined in ../json/procedures) looking for a procedure with the
        corresponding name, and returns the 2 ArchitecturalConstraint object corresponding to the entry and exit
        machine constraints.
        If several procedures share the same name (should NOT happen), only the first one will be returned.

        Args:
            entry_or_exit: whether to return the entry or exit constraints of this procedure
            transition: The related transition
            proc_name (str): The name of the procedure.

        Returns:
            corresponding_entry_proc, corresponding_exit_proc: The 2 corresponding ArchitecturalConstraint objects.

        """

        if entry_or_exit not in ["Entry", "Exit"]:
            raise ValueError(f"""Invalid value for {entry_or_exit} (should be Entry or Exit) """)

        JSON_FOLDER = "../json/procedures/"
        onlyfiles = [f for f in listdir(JSON_FOLDER) if isfile(join(JSON_FOLDER, f))]
        proc_found = False
        corresponding_entry_proc = ArchitecturalConstraints.empty_constraint()
        corresponding_exit_proc = ArchitecturalConstraints.empty_constraint()
        for file in onlyfiles:
            with open(JSON_FOLDER + file, "r") as inFile:
                proc_list = json.load(inFile)
            for proc in proc_list:
                if proc["Name"] == proc_name:
                    try:
                        corresponding_entry_proc = ArchitecturalConstraints(proc["Entry"], transition)
                    except KeyError:
                        pass
                    try:
                        corresponding_exit_proc = ArchitecturalConstraints(proc["Exit"], transition)
                    except KeyError:
                        pass
                    proc_found = True
                    break
            if proc_found:
                break
        if corresponding_entry_proc.is_empty() and corresponding_exit_proc.is_empty():
            raise ValueError("Procedure has no constraints at all, are you sure your name is correct?")
        if entry_or_exit == "Entry":
            return corresponding_entry_proc
        else:
            return corresponding_exit_proc

    def combine_constraints(self, other: "ArchitecturalConstraints") -> "ArchitecturalConstraints":
        """Combines 2 constraint objects by combining all of their subconstraints.
        This first checks whether the constraints are compatible, and will raise an error if not.

        Args:
            other (ArchitecturalConstraints): the constraint to fuse with.

        Returns:
            An architectural constraint object combining both constraints.
        """
        if not self.is_compatible(other):
            raise CombiningIncompatibleConstraintsError(self, other)
        result = ArchitecturalConstraints.empty_constraint()
        result.os_constraints = self.os_constraints + other.os_constraints
        result.account_constraints = self.account_constraints + other.account_constraints
        result.software_constraints = self.software_constraints + other.software_constraints
        result.file_constraints = self.file_constraints + other.file_constraints
        result.assignated_secrets = self.assignated_secrets
        result.assignated_secrets.extend(x for x in other.assignated_secrets if x not in result.assignated_secrets)
        return result

    def fuse_duplicates(self) -> None:
        """Fuses duplicate entries in the architectural constraints.
        This is done by checking for every user constraints if an other user constraint has the same name,
        and combining them into one if so.
        All these constraints should be combinable - if not an incompatibility would have been raised
        This is called at the very end of the refinement (could be done at every step though if you want).
        This also fuses every entry in the OS constraint using the smae process - comparing every constraint and
        returning the smallest one.
        """

        fusion_possible = True
        while fusion_possible:
            to_delete = []
            new_constraints = []
            fusion_possible = False
            for i in range(len(self.os_constraints)):
                for j in range(i + 1, len(self.os_constraints)):
                    if self.os_constraints[i].os_type.is_compatible(
                            self.os_constraints[j].os_type) and not fusion_possible:
                        fusion_possible = True
                        new_constraint = OSConstraints.fuse_duplicates(self.os_constraints[i],
                                                                       self.os_constraints[j])
                        to_delete.append(i)
                        to_delete.append(j)
                        new_constraints.append(new_constraint)
            final_constraints = []
            for i in range(len(self.os_constraints)):
                if i not in to_delete:
                    final_constraints.append(self.os_constraints[i])
            self.os_constraints = final_constraints
            self.os_constraints += new_constraints

        fusion_possible = True
        while fusion_possible:
            to_delete = []
            new_constraints = []
            fusion_possible = False
            for i in range(len(self.account_constraints)):
                for j in range(i + 1, len(self.account_constraints)):
                    if self.account_constraints[i].name == self.account_constraints[j].name and not fusion_possible:
                        fusion_possible = True
                        new_constraint = AccountConstraints.fuse_duplicates(self.account_constraints[i],
                                                                            self.account_constraints[j])
                        to_delete.append(i)
                        to_delete.append(j)
                        new_constraints.append(new_constraint)
            final_constraints = []
            for i in range(len(self.account_constraints)):
                if i not in to_delete:
                    final_constraints.append(self.account_constraints[i])
            self.account_constraints = final_constraints
            self.account_constraints += new_constraints

    @staticmethod
    def empty_constraint() -> "ArchitecturalConstraints":
        """Creates an ArchitecturalConstraints object with no constraints inside.

        Returns:
            An ArchitecturalConstraints object with every attribute empty.

        """
        # Note: Having a special case for ArchitecturalConstraints when Transition is None is not particularly elegant
        # But it works for now
        return ArchitecturalConstraints({"OS": [], "Account": [], "Software": [], "Files": []}, None)

    @staticmethod
    def default_unix_template_with_account(machine_name: str, user_name: str, machine_additional_config: dict) \
            -> "ArchitecturalConstraints":
        """Creates a default ArchitecturalConstraints objects with a few software preinstalled.

        The software are as follows:
        - rsync for file copy purposes
        - acl for script execution purposes

        machine_additional_config is a dictionary. The relevant entry is ["options"], which contains strings.

        Returns:
              An ArchitecturalConstraints object with every attribute empty except software.

        """

        account_constraints = [{
            "Name": user_name,
            "Group": "*",
            "Privilege": "*",
            "Credentials": {"Credential Type": "*", "Requires Secret": False}
        }]
        software_constraints = [
            {
                "Type": "rsync",
                "Version": "*"
            },
            {
                "Type": "acl",
                "Version": "*"
            },
            {"Type": "ufw",
             "Version": "*"
             },
            {"Type": "netcat",
             "Version": "*"
             },
            {"Type": "telnet",
             "Version": "*"
             }
        ]
        file_constraints = []
        if "ornament_users" in machine_additional_config:
            for user in machine_additional_config["ornament_users"]:
                account_constraints.append({
                    "Name": user,
                    "Group": "*",
                    "Privilege": "*",
                    "Credentials": {"Credential Type": "*", "Requires Secret": False}
                })
        final_constraint = ArchitecturalConstraints(
            {"OS": [{"Type": "*", "Version": "*"}], "Account": account_constraints,
             "Software": software_constraints,
             "Files": file_constraints},
            None, machine_name, user_name)

        if "options" in machine_additional_config:
            for option_string in machine_additional_config["options"]:
                option = option_string.split()[0]
                if os.path.isfile(f"./json/options/{option}.json"):
                    with open(f"./json/options/{option}.json") as in_file:
                        constraint = json.load(in_file)
                        if len(option_string.split()) > 1:  # if the yml has to pass arguments
                            for file in constraint["Files"]:
                                if "OPTION_ARG" in file["Content"]["Content Type"]:
                                    file["Content"]["Content Type"] = \
                                        file["Content"]["Content Type"].replace("OPTION_ARG", option_string.split()[1])
                        new_arch_constraint = ArchitecturalConstraints(constraint, None, machine_name, user_name)
                        final_constraint = final_constraint.combine_constraints(new_arch_constraint)
                else:
                    log.warning(f"Unrecognized option: {option}, skipping....")
            if "dns_server" in machine_additional_config:
                with open(f"./json/options/dns_client.json") as in_file:
                    constraint = json.load(in_file)
                    new_arch_constraint = ArchitecturalConstraints(constraint, None, machine_name, user_name)
                    final_constraint = final_constraint.combine_constraints(new_arch_constraint)

        return final_constraint

    @staticmethod
    def empty_constraint_with_account(account_name: str) -> "ArchitecturalConstraints":
        """Creates an ArchitecturalConstraints object with no constraints inside.

            Returns:
                An ArchitecturalConstraints object with every attribute empty.

        """
        account_constraint = {
            "Name": account_name,
            "Group": "*",
            "Privilege": "*",
            "Credentials": {"Credential Type": "*", "Requires Secret": False}
        }
        return ArchitecturalConstraints({"OS": [], "Account": [account_constraint], "Software": [], "Files": []}, None)


class SecretPreconditions:
    """A class handling secret preconditions, which limits which kind of procedures are eligible for a given transition.

    For instance:
    - If a transition requires 1 secret and the procedure doesn't require any, that procedure is inelegible.
    - If a transition requires 1 secret and the procedure may give * amount of them, that procedure is eligible.
    - If a transition requires 1 secret and the procedure may give 1 or more amount of them, that proc is eligible.
    - If a transition requires 1 secret that has already been generated as an SSH_KEY, and the procedure may give 1
    PLAINTEXT_PASSWORD, that proc is ineligible.
    - If a transition requires 1 secret that has already been generated as an SSH_KEY, and the procedure may give 1
    SSH_KEY, that proc is eligible.

    Attributes:
        self.requires_secrets: a list of SecretPrecondition.
        self.rewards_secrets: a list of SecretPrecondition.
    """

    def __init__(self, requires_secrets: list, rewards_secrets: list):
        self.requires_secrets = []
        self.rewards_secrets = []
        for secret_dic in requires_secrets:
            self.requires_secrets.append(subconstraints.SecretPrecondition(secret_dic["Type"], secret_dic["Amount"]))
        for secret_dic in rewards_secrets:
            self.rewards_secrets.append(subconstraints.SecretPrecondition(secret_dic["Type"], secret_dic["Amount"]))

    def is_eligible(self, transition: "sg.Transition", secret_dictionary: dict) -> bool:
        """Checks whether a procedure is eligible for a given transition.

        This is done by checking:
        - If the number of secrets generated or required by the procedure matches the number of secrets generated
        or required by the transition.
        - If the type of secrets generated or required by the transition that have already been generated is a
        subset of the ones applicable to the procedure.

        Args:
            transition (sg.Transition): the transition to check against.
            secret_dictionary(dict): a dictionary containing all secrets that have already been generated.

        Returns:

        """
        nb_requires = len(transition.requires)
        nb_rewards = len(transition.rewards)

        # Get how many secrets our transition can handle by summing all of the secret amounts
        # Returns False if there is not enough to handle the transition
        requires_total = 0
        for requires_secret in self.requires_secrets:
            if requires_secret.amount.is_wildcard():
                requires_total = float('inf')
            else:
                requires_total += int(requires_secret.amount.secret_amount)
        if requires_total < nb_requires:
            return False
        # avoid procedures that specifically require secrets if the transition doesn't
        if requires_total != 0 and nb_requires == 0:
            return False

        rewards_total = 0
        for rewards_secret in self.rewards_secrets:
            if rewards_secret.amount.is_wildcard():
                rewards_total = float('inf')
            else:
                rewards_total += int(rewards_secret.amount.secret_amount)
        if rewards_total < nb_rewards:
            return False

        # avoid procedures that specifically reward secrets if the transition doesn't
        if rewards_total != 0 and nb_rewards == 0:
            return False

        # Get a list of all secret types for the procedure
        proc_requires_type_list = []
        for requires_secret in self.requires_secrets:
            proc_requires_type_list.append(requires_secret.secret_type)

        proc_rewards_type_list = []
        for rewards_secret in self.rewards_secrets:
            proc_rewards_type_list.append(rewards_secret.secret_type)

        # Get a list of all secret types already present in secret_dictionary
        requires_type_list = []
        for secret_name in transition.requires:
            if secret_name in secret_dictionary:
                requires_type_list.append(secret_dictionary[secret_name].secret_type)
        rewards_type_list = []
        for secret_name in transition.rewards:
            if secret_name in secret_dictionary:
                rewards_type_list.append(secret_dictionary[secret_name].secret_type)

        # Check that the ones already generated is a subset of the procedure ones.
        if not set(requires_type_list).issubset(proc_requires_type_list):
            return False
        if not set(rewards_type_list).issubset(proc_rewards_type_list):
            return False
        return True

    def update_secret_dictionary_for_transition(self, secret_dictionary: dict, transition: "sg.Transition",
                                                scenario: "sg.SystemGraph"):
        """Looks for all the secrets handled by the transition and checks that types are coherent for the secrets that
        already exist. Then checks that number matches. If all is correct, updates secret_dictionary with all the
        secrets that were not already in it.
        IMPORTANT: For now this assumes that procedures will at best require or reward a single type of secret.
        While it may be possible to handle more than one type and imagine procedures that could reward more than one
        type (for instance a text leak containing passwords AND ssh keys), it seems just less tedious to add a second
        transition to the graph instead.
        Args:
            secret_dictionary (dict(secg.secret)): the secret dictionary
            transition (sg.Transition): the transition being refined
            scenario: the overall scenario. Useful to get user names related to the secret
        """

        # Get all required and rewards secrets and their type
        required_secrets = transition.requires
        rewarded_secrets = transition.rewards

        # Get the required and rewarded type for the current secret precondition
        if len(self.requires_secrets) == 0:
            required_type = None
        elif len(self.requires_secrets) == 1:
            required_type = self.requires_secrets[0].secret_type
        else:
            raise ValueError  # Not handled ATM

        if len(self.rewards_secrets) == 0:
            rewarded_type = None
        elif len(self.rewards_secrets) == 1:
            rewarded_type = self.rewards_secrets[0].secret_type
        else:
            raise ValueError  # Not handled ATM

        # Check for all the one that exist already in the dictionary that type is coherent
        for required in required_secrets:
            if required in secret_dictionary:
                if secret_dictionary[required].secret_type != required_type:
                    raise ValueError
        for rewarded in rewarded_secrets:
            if rewarded in secret_dictionary:
                if secret_dictionary[rewarded].secret_type != rewarded_type:
                    raise ValueError

        # Check that the numbers check out
        requires_ok = False
        rewards_ok = False

        if len(self.requires_secrets) == 0 and len(required_secrets) == 0:
            requires_ok = True

        if len(self.requires_secrets) == 1:
            if self.requires_secrets[0].amount.secret_amount == "*":
                requires_ok = True
            else:
                try:
                    if int(self.requires_secrets[0].amount.secret_amount) <= len(required_secrets):
                        requires_ok = True
                except ValueError:
                    pass

        if len(self.rewards_secrets) == 0 and len(rewarded_secrets) == 0:
            rewards_ok = True

        if len(self.rewards_secrets) == 1:
            if self.rewards_secrets[0].amount.secret_amount == "*":
                rewards_ok = True
            else:
                try:
                    if int(self.rewards_secrets[0].amount.secret_amount) <= len(rewarded_secrets):
                        rewards_ok = True
                except ValueError:
                    pass

        if not rewards_ok or not requires_ok:
            raise ValueError

        # Add the ones that don't exist already to the dictionary
        for required in required_secrets:
            if required not in secret_dictionary:
                secret_dictionary[required] = secg.Secret(required, required_type, None)
                secret_dictionary[required].add_related_nodes(scenario)
        for rewarded in rewarded_secrets:
            if rewarded not in secret_dictionary:
                secret_dictionary[rewarded] = secg.Secret(rewarded, rewarded_type, None)
                secret_dictionary[rewarded].add_related_nodes(scenario)
