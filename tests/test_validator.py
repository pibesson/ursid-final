"""Tests for the json validator.

"""


import unittest
import chouchen.json_operations as json_operations
import json


class TestValidator(unittest.TestCase):

    def test_schema(self) -> None:
        json_operations.fuse_json_proc_files()
        j = json.load(open(json_operations.OUTPUT))
        assert(json_operations.validate_json_proc(j))
