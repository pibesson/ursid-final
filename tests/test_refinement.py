"""Tests for the procedural refinement."""
import unittest
import chouchen.system_graph as sg
import chouchen.procedural_refinement as pr
import json
import chouchen.json_operations as json_operations


def dummy_scenario() -> sg.SystemGraph:
    """Function to create a simple test scenario.

    Returns:
        A simple scenario containing 2 nodes, 2 transitions and one secret.

    """
    n1 = sg.Node("Bear", "Alice")
    n2 = sg.Node("Bear", "SuperUser")

    t1 = sg.Transition(n1, n2, "T1068")
    t2 = sg.Transition(n2, n2, "T1552", rewards=["skunk_secret"])

    nodes = [n1, n2]
    transitions = [t1, t2]
    starting = [n1]
    victory = [n2]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    return sys_graph


def secret_scenario() -> sg.SystemGraph:
    """A basic scenario using secrets.

    Returns:
        A scenario with 2 nodes and 2 transitions, one requiring and one rewarding the same secret.
    """
    n1 = sg.Node("Bear", "Alice")
    n2 = sg.Node("Bear", "SuperUser")

    t1 = sg.Transition(n1, n1, "T1552", rewards=["skunk_secret"])
    t2 = sg.Transition(n1, n2, "T1078", requires=["skunk_secret"])

    nodes = [n1, n2]
    transitions = [t1, t2]
    starting = [n1]
    victory = [n2]
    sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
    return sys_graph


class TestRefinement(unittest.TestCase):
    def test_simple_scenario(self) -> None:
        scenario = dummy_scenario()
        transitions_left = scenario.transitions
        json_operations.fuse_json_proc_files()
        proc_list = json.load(open(json_operations.OUTPUT))
        arch_constraints = pr.initialize_constraints(scenario)
        secret_dictionary = {}
        success, final_constraints, new_scenario, new_secret_dictionary = \
            pr.refinement_backtracking(scenario, transitions_left, proc_list, arch_constraints, secret_dictionary)
        assert success
