"""Module managing the interfacing with VirtualBox.
In particular, this can stop, start, delete, and get current VM names by calling vboxmanage commands.
"""

import logging
import utils.command as c

log = logging.getLogger(__name__)
vm_name_number = 0


def check_vm_name(vmname: str):
    """
    List VMs and check if a vm already exist with the given name.
    If so, returns a new name by appeding a number on it.
    Used in parser to check whether VMs already exist or not;
    """
    vms = c.command_complex(f"vboxmanage list vms")
    for vm in vms.split("\n"):
        name = vm.split(" ")[0][1:-1]
        if name == vmname:
            global vm_name_number
            vm_name_number += 1
            vmname += str(vm_name_number)
            log.warning(
                f"{name} name already use by virtualbox, suffix added in the non generated one."
            )
            log.warning(f"new name : {vmname}")
    return vmname


def stop_vm(vmname: str):
    """
    Stop VM.
    Currently unused.

    Args:
        vmname: name of the VM to stop.
    """
    c.command_complex(f"vboxmanage controlvm {vmname} poweroff")
    log.debug(f"{vmname} VM halted.")


def start_vm(vmname: str):
    """
    Start VM.
    Currently unused.

    Args:
        vmname: name of the VM to start.
    """
    c.command_complex(f"vboxmanage startvm {vmname} --type=headless")
    log.debug(f"{vmname} VM started.")


def delete_nat(vmname: str):
    """
    Delete nat network.
    Currently unused.

    Args:
        vmname: name of the VM whose network to delete.
    """
    c.command_complex(f"vboxmanage modifyvm {vmname} --nic1=null")
    log.info(f"NAT network removed from {vmname}.")
