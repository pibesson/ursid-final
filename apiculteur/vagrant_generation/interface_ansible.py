"""Utility module to communicate between Ansible, Vagrant and VM objects.
"""
import apiculteur.ansible_generation.interface_vagrant as int_vagrant
from apiculteur.parsing.VM import VirtualMachine


def ansible_provisioning_vm(vm: VirtualMachine) -> str:
    """Write Vagrant instructions to provide the given VM.
    2.0 compatibility mode avoids some stupid error messages.
    Args:
        vm: the virtual machine to provide.
    """
    return f"""
{vm.name}.vm.provision "ansible" do |ansible|
    ansible.compatibility_mode="2.0"
    ansible.playbook = "{int_vagrant.get_vm_playbook_name(vm)}"      
end 
"""


def get_playbook_path(vm: VirtualMachine) -> str:
    """Gets the path of a given VM object's playbook.
    Args:
        vm: the virtual machine whose playbook path to get.
    """
    return int_vagrant.get_vm_playbook_name(vm)
