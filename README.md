# URSID deprecated

The **new version receiving regular updates** is [on this new repository pirat/ursid](https://gitlab.inria.fr/pirat/ursid)


## Welcome to the URSID git repository

This is the official repository for the URSID project.

## What is URSID

URSID is an **automatic vulnerable architecture generation** tool. By describing an attack scenario on a high-level
(using the [MITRE](https://attack.mitre.org/) technique nomenclature), URSID is able to refine it into several on a
procedural level.
All of the resulting architectures will be vulnerable to the same attack on a technical level, but procedures
(such as exploits available on machines) and secrets (such as passwords) will vary on a machine to machine basis.

URSID supports the scientific publication [Automatically Refining a Single Attack Scenario into Multiple Cyber Range Architectures by Pierre-Victor Besson, Valérie Viet Triem Tong, Gilles Guette, Guillaume Piolle, Erwann Abgrall](https://inria.hal.science/hal-04317073) published at the FPS symposium in 2023. 

Uses for URSID as a tool for researchers include:

- Customized honeypot generation, for instance in order to match a known attacker's prefered modus operandi.
- Cyber-range/CTF/red-team training applications, including AI attackers.
- Log and dataset generation corresponding to specific attacks.

## Documentation

The documentation can be found [here](https://ursid.readthedocs.io/en/latest/#).

## Features

- Framework for the formalization of attack scenarios on a technical level.
- Backtracking algorithm to refine one of those scenarios into several instances on a procedural level.
- Deployment of one of these instances into virtual machine architectures.
- An example attack scenario which can be refined, deployed then attacked from start to finish.
- A docker showcase of the refinement process.

## Requirements

- Python 3.10 or later.

If you wish to also deploy the generated architectures:

- Vagrant 2.2.19 or later.
- Ansible. 2.12 or later.
    - Including the community general collection and posix collections.
    - Install with `ansible-galaxy collection install community.general`
      then `ansible-galaxy collection install ansible.posix`.
- VirtualBox v6.1.32 or later.
- Graphviz ([Download page](https://www.graphviz.org/download/)).

## Try it yourself!

### Quick use

If you don't have Vagrant/Ansible/Virtualbox and just wish to see the refinement process:

```bash
pip install -r requirements.txt
python3 main.py ./scenarios/cerbere.json --refinement_only
```

If you have all requirements and wish to deploy the resulting architectures:

```bash
pip install -r requirements.txt
python3 main.py ./scenarios/cerbere.json 
cd output/scenario_0
vagrant up
```

Please refer to the [documentation](https://ursid.readthedocs.io/en/latest/#) for more detailed usage instructions.

## Contributors

- Main contributor and repository maintainer: Pierre-Victor BESSON, PhD student at CentraleSupelec/INRIA.
- Design contributions: Valérie VIET TRIEM TONG, Gilles GUETTE, Erwan ABGRALL and Guillaume PIOLLE, all PhD supervisors.
- Code contribution (Apiculteur): Gireg Maury and Alexandre Monroche, former CentraleSupelec students.
- Special thanks: Alexandre Sanchez.

Questions? Contact pierre-victor.besson@inria.fr
