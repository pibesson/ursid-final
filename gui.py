import os
import subprocess
import sys

from PyQt6.QtGui import *
from PyQt6.QtWidgets import *

import chouchen.graphics as graphics
import chouchen.json_operations as jop
import chouchen.system_graph as sg


# TODO: more than one required/rewarded
# TODO: redirect console input and output to gui

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.scenario = sg.SystemGraph.empty_scenario()
        graphics.draw_graph(self.scenario, technique_names=True, save_mode="../imgs/gui_scenario")

        self.scenario_img = QPixmap("../imgs/gui_scenario.png")
        self.label = QLabel()
        self.label.setPixmap(self.scenario_img)

        self.cerbere_button = QPushButton("CERBERE")
        self.cerbere_button.clicked.connect(self.button_click_cerbere)

        self.save_scenario_button = QPushButton("Save current scenario as json")
        self.save_scenario_button.clicked.connect(self.button_click_save_scenario)

        self.load_scenario_button = QPushButton("Load scenario from json")
        self.load_scenario_button.clicked.connect(self.button_click_load_scenario)

        self.add_node_button = QPushButton("Add node")
        self.add_node_button.clicked.connect(self.button_click_add_node)

        self.add_transition_button = QPushButton("Add Transition")
        self.add_transition_button.clicked.connect(self.button_click_add_transition)

        self.remove_node_button = QPushButton("Remove node")
        self.remove_node_button.clicked.connect(self.button_click_remove_node)

        self.remove_transition_button = QPushButton("Remove transition")
        self.remove_transition_button.clicked.connect(self.button_click_remove_transition)

        self.refinement_button = QPushButton("Refine Scenario")
        self.refinement_button.clicked.connect(self.refine_scenario)

        self.layout = QVBoxLayout()
        self.button_layouts = QHBoxLayout()
        self.button_layouts.addWidget(self.add_node_button)
        self.button_layouts.addWidget(self.remove_node_button)
        self.button_layouts.addWidget(self.add_transition_button)
        self.button_layouts.addWidget(self.remove_transition_button)
        self.button_layouts.addWidget(self.load_scenario_button)
        self.button_layouts.addWidget(self.save_scenario_button)
        self.button_layouts.addWidget(self.cerbere_button)
        self.layout.addLayout(self.button_layouts)
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.refinement_button)
        main_widget = QWidget()
        main_widget.setLayout(self.layout)

        self.setWindowTitle("URSID scenario designer.")
        self.setCentralWidget(main_widget)
        self.show()

    def button_click_cerbere(self):
        self.scenario = self.scenario_cerbere()
        self.update_graphic()

    def button_click_save_scenario(self):
        file_filter = "Json files (*.json)"
        dlg = QFileDialog.getSaveFileName(
            parent=self,
            caption='Select a file',
            directory=os.getcwd(),
            filter=file_filter,
            initialFilter='Json files (*.json)',

        )
        filename, file_extension = os.path.splitext(dlg[0])
        if not file_extension:
            file_extension = ".json"
        sg.export_scenario_as_json(self.scenario, filename + file_extension)

    def button_click_load_scenario(self):
        file_filter = "Json files (*.json)"
        dlg = QFileDialog.getOpenFileName(
            parent=self,
            caption='Select a file',
            directory=os.getcwd(),
            filter=file_filter,
            initialFilter='Json files (*.json)'
        )

        self.scenario = sg.import_scenario_from_json(dlg[0])
        self.update_graphic()

    def update_graphic(self):
        graphics.draw_graph(self.scenario, technique_names=True, save_mode="../imgs/gui_scenario")
        self.label.setPixmap(QPixmap("../imgs/gui_scenario.png"))

    def button_click_add_node(self):
        dlg = NodeDialog(self.scenario, self)
        if dlg.exec():
            machine, user, is_starting, is_winning = dlg.get_values()
            self.scenario.add_node_to_scenario(machine, user, is_starting, is_winning)
            self.update_graphic()
            print("Success!")

    def button_click_remove_node(self):
        dlg = RemoveNodeDialog(self.scenario, self)
        if dlg.exec():
            node = dlg.get_node()
            self.scenario.remove_node_from_scenario(node)
            self.update_graphic()
            print("Success!")

    def button_click_add_transition(self):
        dlg = TransitionDialog(self.scenario, self)
        if dlg.exec():
            entry_node, exit_node, technique, required, rewarded = dlg.get_values()
            self.scenario.add_transition_to_scenario(entry_node, exit_node, technique, required, rewarded)
            self.update_graphic()
            print("Success!")

    def button_click_remove_transition(self):
        dlg = RemoveTransitionDialog(self.scenario, self)
        if dlg.exec():
            transition = dlg.get_transition()
            self.scenario.remove_transition_from_scenario(transition)
            self.update_graphic()
            print("Success!")

    def refine_scenario(self):
        dlg = RefinementDialog(self.scenario, self)
        if dlg.exec():
            number, refinement_only, no_nat, ansible_ignore_errors = dlg.get_values()
            print(number, refinement_only, no_nat, ansible_ignore_errors)
            sg.export_scenario_as_json(self.scenario, "scenarios/gui_scenario.json")
            input_string = f"python3 ./main.py ./scenarios/gui_scenario.json -n {number}"
            if refinement_only:
                input_string += " --refinement_only"
            if no_nat:
                input_string += " --no-nat"
            if ansible_ignore_errors:
                input_string += " --ansible-ignore-errors"
            subprocess.Popen(input_string, shell=True)

    @staticmethod
    def scenario_cerbere():
        n1 = sg.Node("Attacker", "SuperUser")
        n2 = sg.Node("Zagreus", "Alice")
        n3 = sg.Node("Zagreus", "SuperUser")
        n4 = sg.Node("Hades", "Bob")
        n5 = sg.Node("Hades", "SuperUser")
        n6 = sg.Node("Melinoe", "postgres")
        t1 = sg.Transition(n1, n2, "T1190")
        t2 = sg.Transition(n2, n3, "T1068")
        t3 = sg.Transition(n3, n3, "T1552", rewards=["zagreus_secret"])
        t4 = sg.Transition(n1, n4, "T1190", requires=["zagreus_secret"], rewards=["hades_secret"])
        t5 = sg.Transition(n1, n5, "T1021", requires=["hades_secret"])
        t6 = sg.Transition(n5, n5, "T1552", rewards=["melinoe_db_password"])
        t7 = sg.Transition(n5, n6, "T1021", requires=["melinoe_db_password"], rewards=["melinoe_flag"])
        nodes = [n1, n2, n3, n4, n5, n6]
        transitions = [t1, t2, t3, t4, t5, t6, t7]
        starting = [n1]
        victory = [n6]
        sys_graph = sg.SystemGraph(nodes, starting, victory, transitions)
        return sys_graph


class RemoveNodeDialog(QDialog):
    def __init__(self, scenario, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Remove node!")
        self.scenario = scenario
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.text = QLabel("Note: this will also remove any transitions attached to this node.")
        self.nodes = QComboBox()
        for node in self.scenario.nodes:
            self.nodes.addItem(f"p{node.node_id}: {str(node)}")

        self.error_message = QLabel("")
        self.error_message.setStyleSheet("color: red;")
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.nodes)
        self.layout.addWidget(self.error_message)
        self.layout.addWidget(self.buttonBox)

        self.check_button_status()
        self.setLayout(self.layout)

    def get_node(self):
        node_id = int(self.nodes.currentText().split(":")[0][1:])
        for node in self.scenario.nodes:
            if node.node_id == node_id:
                return node

    def check_button_status(self):
        # Check if node is valid by checking other nodes values
        # Adds a red error missage if invalid.
        # Unoptimized but easier to read than with a big boolean expression
        grey_button = True
        if self.nodes.currentText() == "":
            grey_button = False
        if grey_button:
            self.error_message.setText("")
        self.buttonBox.buttons()[0].setEnabled(grey_button)


class RemoveTransitionDialog(QDialog):
    def __init__(self, scenario, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Remove transition!")
        self.scenario = scenario
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.transitions = QComboBox()
        for transition in self.scenario.transitions:
            self.transitions.addItem(f"t{transition.transition_id}: {str(transition)}")

        self.error_message = QLabel("")
        self.error_message.setStyleSheet("color: red;")
        self.layout.addWidget(self.transitions)
        self.layout.addWidget(self.error_message)
        self.layout.addWidget(self.buttonBox)

        self.check_button_status()
        self.setLayout(self.layout)

    def get_transition(self):
        transition_id = int(self.transitions.currentText().split(":")[0][1:])
        for transition in self.scenario.transitions:
            if transition.transition_id == transition_id:
                return transition

    def check_button_status(self):
        # Check if node is valid by checking other nodes values
        # Adds a red error missage if invalid.
        # Unoptimized but easier to read than with a big boolean expression
        grey_button = True
        if self.transitions.currentText() == "":
            grey_button = False
        if grey_button:
            self.error_message.setText("")
        self.buttonBox.buttons()[0].setEnabled(grey_button)


class NodeDialog(QDialog):
    def __init__(self, scenario, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Add new node!")
        self.scenario = scenario
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.machine = QLineEdit("")
        self.machine.setPlaceholderText("Machine")
        self.user = QLineEdit("")
        self.user.setPlaceholderText("User")
        self.is_starting = QCheckBox("Starting node.")
        self.is_winning = QCheckBox("Winning node.")

        self.machine.textChanged.connect(self.check_button_status)
        self.user.textChanged.connect(self.check_button_status)
        self.is_starting.stateChanged.connect(self.check_button_status)
        self.is_winning.stateChanged.connect(self.check_button_status)

        self.error_message = QLabel("")
        self.error_message.setStyleSheet("color: red;")
        self.layout.addWidget(self.machine)
        self.layout.addWidget(self.user)
        self.layout.addWidget(self.is_starting)
        self.layout.addWidget(self.is_winning)
        self.layout.addWidget(self.error_message)
        self.layout.addWidget(self.buttonBox)
        # self.check_button_status()
        self.setLayout(self.layout)

    def check_button_status(self):
        # Check if node is valid by checking other nodes values
        # Adds a red error missage if invalid.
        # Unoptimized but easier to read than with a big boolean expression
        grey_button = True
        for node in self.scenario.nodes:
            if node.machine.casefold() == self.machine.text().casefold() \
                    and node.user.casefold() == self.user.text().casefold():
                self.error_message.setText("Node already exists.")
                self.error_message.update()
                grey_button = False
        if self.is_winning.isChecked() and self.is_starting.isChecked():
            self.error_message.setText("Node can't be both starting and winning.")
            grey_button = False
        if self.machine.text() == "" or self.user.text() == "":
            self.error_message.setText("Missing machine or user.")
            grey_button = False
        if grey_button:
            self.error_message.setText("")
        self.buttonBox.buttons()[0].setEnabled(grey_button)

    def get_values(self):
        return self.machine.text(), self.user.text(), self.is_starting.isChecked(), self.is_winning.isChecked()


class TransitionDialog(QDialog):
    def __init__(self, scenario, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Add new transition!")
        self.scenario = scenario
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.entry_node = QComboBox()
        self.exit_node = QComboBox()
        for node in scenario.nodes:
            self.entry_node.addItem(str(node))
            self.exit_node.addItem(str(node))
        self.technique = QComboBox()
        for tech in jop.get_all_available_technique_names():
            self.technique.addItem(tech)

        self.required_secrets = QComboBox()
        self.rewarded_secrets = QComboBox()
        self.required_layout = QHBoxLayout()
        self.required_label = QLabel("Required:")
        self.required_secrets.addItem("None")
        self.required_secrets.setEditable(True)
        self.rewarded_secrets.addItem("None")
        self.rewarded_secrets.setEditable(True)
        self.rewarded_layout = QHBoxLayout()
        self.rewarded_label = QLabel("Rewarded:")
        for transition in scenario.transitions:
            for required in transition.requires:
                if self.required_secrets.findText(required) == -1:
                    self.rewarded_secrets.addItem(required)
            for rewarded in transition.rewards:
                if self.rewarded_secrets.findText(rewarded) == -1:
                    self.required_secrets.addItem(rewarded)

        self.error_message = QLabel("")
        self.error_message.setStyleSheet("color: red;")

        self.required_layout.addWidget(self.required_label)
        self.required_layout.addWidget(self.required_secrets)
        self.rewarded_layout.addWidget(self.rewarded_label)
        self.rewarded_layout.addWidget(self.rewarded_secrets)
        self.layout.addWidget(self.entry_node)
        self.layout.addWidget(self.exit_node)
        self.layout.addWidget(self.technique)
        self.layout.addLayout(self.required_layout)
        self.layout.addLayout(self.rewarded_layout)
        self.layout.addWidget(self.error_message)
        self.layout.addWidget(self.buttonBox)

        self.check_button_status()
        self.setLayout(self.layout)

    def check_button_status(self):
        # For now is always valid
        grey_button = True
        if grey_button:
            self.error_message.setText("")
        self.buttonBox.buttons()[0].setEnabled(grey_button)

    def get_values(self):
        return self.entry_node.currentText(), self.exit_node.currentText(), self.technique.currentText(), \
            self.required_secrets.currentText(), self.rewarded_secrets.currentText()


class RefinementDialog(QDialog):
    def __init__(self, scenario, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Refine scenario!")
        self.scenario = scenario
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()

        self.number_box = QHBoxLayout()
        self.number_label = QLabel("Number of output scenarios (max 50):")
        self.number_spinbox = QSpinBox()
        self.number_spinbox.setRange(1, 50)
        self.number_box.addWidget(self.number_label)
        self.number_box.addWidget(self.number_spinbox)

        self.refinement_only = QCheckBox("Only perform refinement (no Ansible/Vagrant)")

        self.no_nat = QCheckBox("Remove NAT")
        self.ansible_ignore_errors = QCheckBox("Ignore Ansible Errors")
        self.error_message = QLabel("")
        self.error_message.setStyleSheet("color: red;")

        self.refinement_only.stateChanged.connect(self.check_button_status)

        self.layout.addLayout(self.number_box)
        self.layout.addWidget(self.refinement_only)
        # self.layout.addLayout(self.output_directory_box)
        self.layout.addWidget(self.no_nat)
        self.layout.addWidget(self.ansible_ignore_errors)
        self.layout.addWidget(self.error_message)
        self.layout.addWidget(self.buttonBox)

        self.setLayout(self.layout)
        self.check_button_status()

    def check_button_status(self):
        # Disables some options if refinement_only was checked
        grey_button = True
        if self.refinement_only.isChecked():
            grey_button = False

        if not grey_button:
            self.error_message.setText("")
            self.no_nat.setChecked(False)
            self.ansible_ignore_errors.setChecked(False)
            self.no_nat.update()
            self.ansible_ignore_errors.update()
        self.no_nat.setEnabled(grey_button)
        self.ansible_ignore_errors.setEnabled(grey_button)

    def get_values(self):
        return self.number_spinbox.text(), self.refinement_only.isChecked(), \
            self.no_nat.isChecked(), self.ansible_ignore_errors.isChecked()

    def pick_directory(self):
        file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        self.output_directory_label.setText(file)
        self.output_directory_label.update()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()
